import { Component, OnInit, ViewEncapsulation, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-date-picker',
  templateUrl: './date-picker.component.html',
  styleUrls: ['./date-picker.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class DatePickerComponent implements OnInit {
  @Output() date = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  onUpdate(event) {
    this.date.emit(event);
  }

}
