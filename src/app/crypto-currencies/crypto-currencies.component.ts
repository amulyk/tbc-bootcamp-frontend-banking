import { Component, OnInit } from '@angular/core';
import { CryptoCurrenciesService } from '../services/crypto-currencies.service';
import { FormBuilder } from '@angular/forms';


interface ICryptoCurrenciesComponent {
  Name: string;
  Price: string;
  MarketCap: string;
  Volume: string;
  TotalVol: string;
  Changes24H: number;
  Changes7D: number;
  Image: string;
  Symbol: string;
}

@Component({
  selector: 'app-crypto-currencies',
  templateUrl: './crypto-currencies.component.html',
  styleUrls: ['./crypto-currencies.component.scss']
})
export class CryptoCurrenciesComponent implements OnInit {
  searchForm;
  cryptocurrencies$;
  list = [{ value: 'USD' }, { value: 'EUR' }];
  cryptoheaders = ['Name & Symbol', 'Price', 'Market Cap', 'Volume', 'Total Vol.', 'Chg 24h', 'Chg 7d'];
  array = [];

  constructor(private cryptoCurrenciesService: CryptoCurrenciesService, private formBulder: FormBuilder) {
    this.searchForm = this.formBulder.group({
      search: '',
      currency: 'USD'
    });
    this.searchForm.valueChanges.subscribe((value: { search: string, currency: string }) => {
      this.cryptocurrencies$ = this.cryptoCurrenciesService.getCryptoCurrencies(value.currency, value.search)
      .subscribe((item: ICryptoCurrenciesComponent[]) => {
        this.array = item;
      });
    });
  }

  ngOnInit() {
    this.cryptocurrencies$ = this.cryptoCurrenciesService.getCryptoCurrencies('USD').subscribe(value => {
      this.array = value;
    });
  }
}
