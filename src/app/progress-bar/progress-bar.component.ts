import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { Subject } from 'rxjs';
import { LoaderService } from './../services/loader.service';

@Component({
  selector: 'app-progress-bar',
  templateUrl: './progress-bar.component.html',
  styleUrls: ['./progress-bar.component.scss'],
  animations: [
    trigger('load', [
      state('start', style({
        width: '{{endPoint}}%'
      }), {params: {endPoint: '0%'}}),
      state('change', style({
        width: '{{endPoint}}%'
      }), {params: {endPoint: '0%'}}),
      transition('* <=> *', animate('1s'))
    ])
  ]
})
export class ProgressBarComponent implements OnInit, OnChanges {
  @Input() endPoint: number;
  @Input() color: string;
  @Input() loader = false;
  changed: boolean;

  isLoading: Subject<boolean> = this.loaderService.isLoading;
  constructor(private loaderService: LoaderService) {
  }

  ngOnInit() {
    this.changed = false;
  }

  ngOnChanges() {
    this.changed = !this.changed;
  }
}

