import { Component, EventEmitter, OnInit, Output, Input } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-bank-transfer',
  templateUrl: './bank-transfter.component.html',
  styleUrls: ['./bank-transfter.component.scss']
})
export class BankTransfterComponent implements OnInit {
  form;
  @Output() transaction = new EventEmitter();
  @Input() transferTypes;
  @Input() currencies;
  @Input() cards;

  constructor(private fb: FormBuilder) {
  }

  ngOnInit() {
    this.form = this.fb.group({
      account: [this.cards[0] && this.cards[0].value || null, Validators.required],
      transferTo: ['', Validators.required],
      beneficiary: ['', Validators.required],
      amount: [0, Validators.required],
      currency: ['USD', Validators.required],
      transferType: [this.transferTypes[0].value, Validators.required]
    });
  }

  onSubmit() {
    if (!this.form.invalid) {
      const card = this.cards.find(item => {
        return item.value === this.form.value.account;
      });
      const sendData = {...this.form.value, account: card && card.objectId};
      this.transaction.emit(sendData);
    }
  }
}
