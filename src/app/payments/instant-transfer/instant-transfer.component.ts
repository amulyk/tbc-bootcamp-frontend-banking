import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-instant-transfer',
  templateUrl: './instant-transfer.component.html',
  styleUrls: ['./instant-transfer.component.scss']
})
export class InstantTransferComponent implements OnInit {
  form;
  @Output() transaction = new EventEmitter();
  @Input() transferTypes;
  @Input() cards;

  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.form = this.fb.group({
      account: [this.cards[0] && this.cards[0].value || null, Validators.required],
      transferTo: ['', Validators.required],
      amount: [0, Validators.required],
      transferType: [this.transferTypes[0].value, Validators.required],
    });
  }

  onSubmit() {
    if (!this.form.invalid) {
      const card = this.cards.find(item => {
        return item.value === this.form.value.account;
      });
      const sendData = {...this.form.value, account: card && card.objectId};
      this.transaction.emit(sendData);
    }
  }

}
