import { Component, EventEmitter, OnInit, Output, Input } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-online-payment',
  templateUrl: './online-payment.component.html',
  styleUrls: ['./online-payment.component.scss']
})
export class OnlinePaymentComponent implements OnInit {
  form;
  @Output() transaction = new EventEmitter();
  @Input() paymentSystems;
  @Input() currencies;
  @Input() cards;

  constructor(private fb: FormBuilder) {
  }

  ngOnInit() {
    this.form = this.fb.group({
      account: [this.cards[0] && this.cards[0].value || null, Validators.required],
      paymentSystem: [this.paymentSystems[0].value, Validators.required],
      payPalAccount: ['', Validators.required],
      amount: [0, Validators.required],
      currency: ['USD', Validators.required]
    });
  }

  onSubmit() {
    if (!this.form.invalid) {
      const card = this.cards.find(item => {
        return item.value === this.form.value.account;
      });
      const sendData = {...this.form.value, account: card && card.objectId};
      this.transaction.emit(sendData);
    }
  }

  onCancel() {
    this.form.reset();
  }
}
