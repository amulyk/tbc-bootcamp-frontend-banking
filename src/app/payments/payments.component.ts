import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { CardService } from '../services/card.service';
import { Notify } from '../services/notify.service';
import { TransactionService } from '../services/transaction.service';
import { ICard } from '../interfaces/card';
import { NotificationService } from '../services/notification.service';

@Component({
  selector: 'app-payments',
  templateUrl: './payments.component.html',
  styleUrls: ['./payments.component.scss'],
})
export class PaymentsComponent implements OnInit {
  public currencies = [{ value: 'USD' }];
  public transferTypes = [{ value: 'Personal transfer' }, { value: 'Non personal transfer' }];
  public paymentSystems = [{ value: 'PayPal' }, { value: 'TBC' }];
  form;
  cards;
  provider;
  images = {
    instant: '&#xf109',
    bank: '&#xf12d',
    online: '&#xf2af',
  };
  types = {
    instant: 'instant transfer',
    bank: 'bank transfer',
    online: 'electronic payments',
  };
  constructor(
    private cardService: CardService,
    private notifyService: Notify,
    private transService: TransactionService,
    private formBuilder: FormBuilder,
    private notificationService: NotificationService
  ) {
  }

  ngOnInit() {
    this.form = this.formBuilder.group({
      search: ['']
    });

    this.cardService.getCards().subscribe(cards => {
      this.cards = cards.map(card => {
        return { ...card, value: card.cardTypes.original };
      });
      this.provider = 'online';
    });
  }

  setProvider(provider) {
    this.provider = provider;
  }

  get title() {
    if (this.provider) {
      return this.provider[0].toUpperCase() + this.provider.substring(1) + ' transfer';
    }

    return 'Transfer';
  }

  onTransfer(formValue) {
    const notifyInfo = {
      title: 'Make payment',
      image: 'test image',
      message: 'test message'
    };
    const transInfo = {
      amount: +formValue.amount,
      cardId: formValue.account,
      receiverId: 'null',
      image: this.images[this.provider],
      type: this.types[this.provider],
      title: this.provider.toUpperCase() + ' Transation'
    };
    const card = this.cards.find(item => item.objectId === formValue.account);
    const cardInfo = {
      amount: card.amount - formValue.amount,
      objectId: card.objectId,
    };

    const senderOption = { transInfo, notifyInfo, cardInfo };

    if (this.provider === 'instant') {
      const notifyInfo = {
        title: 'Get money',
        image: 'test image',
        message: 'You get money from ' + card.objectId
      };
      this.cardService.getCard(formValue.transferTo).subscribe((value: ICard) => {
        if (value.objectId) {
          transInfo.receiverId = value.customerId;
          const cardInfo = {
            amount: value.amount + Number(formValue.amount),
            objectId: value.objectId,
          };
          const options = { notifyInfo, cardInfo };
          this.putMoney(options);
          this.sendMoney(senderOption);
          this.notificationService.push({text: 'Money sent'});
        } else {
          this.notificationService.push({text: 'Wrong acoount id'});
        }
      });
    } else {
      this.notificationService.push({text: 'Money sent'});
      this.sendMoney(senderOption);
    }
  }

  putMoney(options) {
    this.notifyService.notify(options.notifyInfo).subscribe(value => value);
    this.cardService.updateAmmount(options.cardInfo.objectId, options.cardInfo.amount).subscribe(value => value);
  }

  sendMoney(options) {
    this.notifyService.notify(options.notifyInfo).subscribe(value => value);
    this.transService.makeTransation(options.transInfo).subscribe(value => value);
    this.cardService.updateAmmount(options.cardInfo.objectId, options.cardInfo.amount).subscribe(value => value);
  }

}
