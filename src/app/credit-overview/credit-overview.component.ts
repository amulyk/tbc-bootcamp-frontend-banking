import { Component, OnInit, Input } from '@angular/core';
import { state, style, trigger, transition, animate } from '@angular/animations';

@Component({
  selector: 'app-credit-overview',
  templateUrl: './credit-overview.component.html',
  styleUrls: ['./credit-overview.component.scss'],
  animations: [
    trigger('popinPopout', [
      state('popin', style({
        'stroke-width': 46
      })),
      state('popout', style({
        'stroke-width': 59
      })),
      transition('* => *', [
        animate(200)
      ])
    ]),
    trigger('showHide', [
      state('show', style({
        opacity: 1,
        'z-index': '1'
      })),
      state('hide', style({
        opacity: 0,
        'z-index': '-1'
      })),
      transition('* => show', [
        animate(200)
      ]),
      transition('* => hide', [
        animate(50)
      ])
    ])
  ]
})
export class CreditOverviewComponent implements OnInit {
  @Input() amount: number;
  @Input() interestRate: number;
  @Input() paid: number;
  public total: number;

  public poppedOut: {
    name: 'amount' | 'interestRate' | 'paid',
    value: number
  } | null;

  popout(name) {
    this.poppedOut = { name, value: this[name] };
  }

  popin() {
    this.poppedOut = null;
  }

  ngOnInit() {
    this.total = this.amount + this.interestRate + this.paid;
  }

  constructor() { }
}
