import { Component, OnInit, ElementRef } from '@angular/core';
import {
  animate,
  state,
  style,
  trigger,
  transition
} from '@angular/animations';
import { LoginService } from '../login.service';
import { ICustomer } from '../login.interface';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header-profile',
  templateUrl: './header-profile.component.html',
  styleUrls: ['./header-profile.component.scss'],
  animations: [
    trigger('animate', [
      state('open', style({
        top: '45px',
        visibility: 'visible',
        opacity: 1,
      })),
      state('close', style({
        top: '52px',
        visibility: 'hidden',
        opacity: 0,
      })),
      transition('open => close', animate(150)),
      transition('close => open', animate(100))
    ])
  ],
  host: {
    '(document:click)': 'onClick($event)'
  }
})
export class HeaderProfileComponent implements OnInit {
  name: string;
  email: string;
  gender: string;

  show = false;

  constructor(
    private _eref: ElementRef,
    private router: Router,
    private loginService: LoginService
  ) { }

  ngOnInit() {
    this.setUser();

    document.addEventListener('settingsUpdate', () => {
      this.setUser();
    }, false);
  }

  onClick(event) {
    if (!this._eref.nativeElement.contains(event.target)) {
      this.hideProfile();
    }
  }

  showProfile() {
    if (this.show) {
      this.hideProfile();
    } else {
      this.show = true;
    }
  }

  hideProfile() {
    this.show = false;
  }

  setUser() {
    this.loginService.customer
      .subscribe((data: ICustomer) => {
        const { email, firstName, sex, lastName } = data;
        this.name = `${firstName ? firstName : ''} ${lastName ? lastName : ''}`;
        this.email = email;
        this.gender = sex;
        this.setIcon();
      });
  }

  setIcon() {
    const icons = document.querySelectorAll('.profile-pic i');
    for (let i = 0; i < icons.length; i++) {
      if (this.gender === 'male') {
        icons[i].className = 'la la-male';
      } else if (this.gender === 'female') {
        icons[i].className = 'la la-female';
      }
    }
  }

  logout() {
    this.loginService.logout();
    localStorage.removeItem('UserProperties');
    this.router.navigate(['/login']);
  }

}