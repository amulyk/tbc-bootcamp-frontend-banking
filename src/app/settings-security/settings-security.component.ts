import { Component, OnInit } from '@angular/core';
import { SettingsSecuretyService } from '../settings-securety.service';
import {
  FormBuilder,
  Validators,
  FormControl,
} from '@angular/forms';

@Component({
  selector: 'app-settings-security',
  templateUrl: './settings-security.component.html',
  styleUrls: ['./settings-security.component.scss'],
})
export class SettingsSecurityComponent implements OnInit {

  securetyForm;
  list = [
    { value: 'Select Question' },
    { value: 'What is your friend\'s name' },
    { value: 'Which city were you born' },
    { value: 'Favourite character from movie' },
    { value: 'Name of your favourite song' }
  ];
  list2 = [...this.list];
  questionAndAnswers = {};
  customer;
  deleted = [];
  users$;

  errorMesage1; // we selected secon question but not answered
  errorMesage2; // we changed second answer and not selected question
  errorMesage3; // first question is not selected
  // canSubmit = true;
  constructor(
    private formBuilder: FormBuilder,
    public securetyService: SettingsSecuretyService,
  ) {
    this.securetyForm = formBuilder.group({
      answerFirst: ['', [Validators.required, Validators.pattern(/[\w0-9]/)]],
      answerSecond: ['', [Validators.pattern(/[\w0-9]/)]],
      question1: [''],
      question2: ['']
    }
    );


    // this.question2.valueChanges.subscribe((val) => {
    //   if (this.answerSecond.invalid || this.answerSecond.value === '') {
    //     this.canSubmit = false;
    //   }
    // });
    this.answerFirst.valueChanges.subscribe((val) => {
      this.errorMesage3 = undefined;
    });

    this.answerSecond.valueChanges.subscribe((val) => {
      this.errorMesage1 = undefined;
      this.errorMesage2 = undefined;
    });

  }

  ngOnInit() {
    const user = this.securetyService.get().subscribe((data) => {
      this.customer = data;

      this.securetyForm.get('answerFirst').setValue(this.customer.securityAnswer1);
      this.securetyForm.get('answerSecond').setValue(this.customer.securityAnswer2);
      this.securetyForm.get('question1').setValue(this.customer.securityQuestion1);
      this.securetyForm.get('question2').setValue(this.customer.sequrityQuestion2);
    });
  }
  get isTouched() {
    return this.answerFirst.value !== this.customer.securityAnswer1
      ||
      this.answerSecond.value !== this.customer.securityAnswer2
      ||
      this.question1.value !== this.customer.securityQuestion1
      ||
      this.question2.value !== this.customer.sequrityQuestion2;
  }

  get values() {
    return this.securetyForm.controls;
  }

  get answerFirst() {
    return this.securetyForm.get('answerFirst') as FormControl;
  }
  get answerSecond() {
    return this.securetyForm.get('answerSecond') as FormControl;
  }
  get question1() {
    return this.securetyForm.get('question1') as FormControl;
  }

  get question2() {
    return this.securetyForm.get('question2') as FormControl;
  }

  get isValid() {
    return this.securetyForm.valid;
  }

  reset() {
    this.securetyForm.reset();
    const user = this.securetyService.get().subscribe((data) => {
      this.customer = data;

      this.securetyForm.get('answerFirst').setValue(this.customer.securityAnswer1);
      this.securetyForm.get('answerSecond').setValue(this.customer.securityAnswer2);
      this.securetyForm.get('question1').setValue(this.customer.securityQuestion1);
      this.securetyForm.get('question2').setValue(this.customer.sequrityQuestion2);
    });
  }

  arrangeLists() {
    this.list2 = [...this.list];
    for (const i of this.list) {
      if ((i.value === this.question1.value || i.value === this.question2.value) && i.value !== 'Select Question') {
        this.list2.splice(this.list2.indexOf(i), 1);
      }
    }
  }

  onSubmit(form) {
    if (this.question2.value !== 'Select Question' && this.answerSecond.value === '') {
      this.errorMesage1 = 'Fill answer or select \'Select Question\'';

      return;

    } else if (this.answerSecond.value !== '' && this.question2.value === 'Select Question') {
      this.errorMesage2 = 'Choose question#2 or clear Answer for secon question';

      return;
    } else if (this.question1.value === 'Select Question') {
      this.errorMesage3 = 'Please select question';

      return;
    }
    if (this.securetyForm.status === 'VALID') {

      // if we send data reset all errors
      this.errorMesage1 = undefined;
      this.errorMesage2 = undefined;
      this.errorMesage3 = undefined;

      this.securetyService.update({
        securityAnswer2: this.answerSecond.value,
        securityQuestion1: this.question1.value,
        securityAnswer1: this.answerFirst.value,
        sequrityQuestion2: this.question2.value,

      }).subscribe(() => {
        this.reset();
      });
    } else {
      window.alert('Fill required fields');
    }
  }
}
