import { Component, Input, OnChanges, ElementRef, ViewChild } from '@angular/core';
import { Chart } from 'chart.js';
import { trigger, state, style, transition, animate } from '@angular/animations';

@Component({
  selector: 'app-dashboard-income-line-chart',
  templateUrl: './dashboard-income-line-chart.component.html',
  styleUrls: ['./dashboard-income-line-chart.component.scss'],
  animations: [
    trigger('dialog', [
      state('on', style({
        transform: 'scale(1)',
        display: 'block',
      })),
      state('off', style({
        transform: 'scale(0.8)',
        display: 'none',
      })),
      transition('off <=> on', animate(150)),
    ])
  ]
})
export class DashboardIncomeLineChartComponent implements OnChanges {
  chart;
  status = true;

  @ViewChild('canvas', { static: true }) public canvas: ElementRef;

  private cx: CanvasRenderingContext2D;

  @Input() data;
  constructor() { }

  toggleStatus() {
    this.status = !this.status;
  }

  ngOnChanges() {
    this.createCanvas(this.data);
  }

  createCanvas(data) {
    const canvas: HTMLCanvasElement = this.canvas.nativeElement;
    this.cx = canvas.getContext('2d');

    const ctx = this.cx;
    Chart.defaults.global.defaultFontFamily = '\'Roboto\', sans-serif';
    const gradientFill1 = ctx.createLinearGradient(0, 0, 0, 230);
    gradientFill1.addColorStop(0, 'rgba(255, 171, 43, .2)');
    gradientFill1.addColorStop(1, 'rgba(255, 171, 43, 0)');

    const gradientFill2 = ctx.createLinearGradient(0, 0, 0, 230);
    gradientFill2.addColorStop(0, 'rgba(77, 124, 254, .1)');
    gradientFill2.addColorStop(1, 'rgba(77, 124, 254, 0)');

    const gradientFill3 = ctx.createLinearGradient(0, 0, 0, 230);
    gradientFill3.addColorStop(0, 'rgba(232, 236, 239, 1)');
    gradientFill3.addColorStop(1, 'rgba(242, 244, 246, .4)');

    this.chart = new Chart('line-chart-canvas', {
      type: 'line',
      data: {
        labels: data.labels,
        datasets: [
          {
            label: 'Income',
            data: data.income,
            borderColor: '#FFAB2B',
            fill: true,
            backgroundColor: gradientFill1,
            pointRadius: 0,
            pointHitRadius: 12,
            pointHoverRadius: 8,
            pointBorderColor: '#A7BEFF',
            hoverBorderColor: '#FDD08C',
            hoverBorderWidth: 5,
            hoverBackgroundColor: '#FFAB2B',
          },
          {
            label: 'Outcome',
            data: data.outcome,
            borderColor: '#4D7CFE',
            fill: true,
            backgroundColor: gradientFill2,
            pointRadius: 0,
            pointHitRadius: 12,
            pointHoverRadius: 8,
            pointBorderColor: '#A7BEFF',
            hoverBorderColor: '#A7BEFF',
            hoverBorderWidth: 5,
            hoverBackgroundColor: '#4D7CFE',
          },
        ]
      },
      options: {

        tooltips: {
          displayColors: false,
          backgroundColor: 'white',
          titleFontColor: '#252631',
          titleFontStyle: '400',
          titleFontSize: 14,
          caretSize: 7.5,
          caretPadding: 15,
          bodyFontColor: 'black',
          xPadding: 15,
          yPadding: 11,
          bodyFontSize: 14,
          bodyFontStyle: '500',
          borderColor: '#F2F4F6',
          borderWidth: 1,
          callbacks: {
            label(tooltipItem) {
              return '$ ' + tooltipItem.yLabel;
            },
            title(tooltipItem) {
              return data.labelsFull[tooltipItem[0].index];
            }
          }
        },
        responsive: false,
        legend: {
          display: false,
          position: 'top',
          labels: {
            boxWidth: 5,
            usePointStyle: true,
            fontColor: '#252631',
            fontSize: 14,
            fontFamily: 'Rubik'
          }
        },
        scales: {
          xAxes: [{
            ticks: {
              fontColor: 'black',
              fontSize: 14,
              fontStyle: '200',
              autoSkip: true,
              maxTicksLimit: 6,
              padding: 8,
            },
            display: true,
            gridLines: {
              borderDash: [10, 4],
              color: gradientFill3,
            }
          }],
          yAxes: [{
            display: false,
            ticks: {
              beginAtZero: true,
              max: data.max
            }
          }],
        }
      }
    });
  }

}
