import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { trigger, state, style } from '@angular/animations';

@Component({
  selector: 'app-stocks',
  templateUrl: './stocks.component.html',
  styleUrls: ['./stocks.component.scss'],
  animations: [
    trigger('openClose', [
      state('open', style ({
        color: '#FFAB2B',
      })),
      state('close', style ({
        color: '#979797',
      }))
    ]),
    trigger('posNeg', [
      state('pos', style({
        color: '#6DD230'
      })),
      state('neg', style({
        color: '#FE4D97'
      }))
    ])
  ]
})
export class StocksComponent implements OnInit {

  constructor(private http: HttpClient) { }

  url = 'https://api.worldtradingdata.com/api/v1/stock?symbol=AAPL,MSFT,HSBA.L';
  apiKey = '&api_token=dQ8OTyfaFtGhRUOBQmEy6CImS2Gce3sVeXcURWhU2O869ojh8prXR1A8ssX9';
  dataFav = [];
  datas = [];

  ngOnInit() {
    this.http.get(`${this.url}${this.apiKey}`)
    .subscribe((value: any) => {
      this.datas = value.data;
    });
  }

  getData() {}

  addFav(value) {

    if (!this.dataFav.includes(value)) {
      value.id = true;
      this.dataFav.push(value);
    } else {
      value.id = false;
      this.dataFav.splice(this.dataFav.indexOf(value), 1);
    }
  }
}
