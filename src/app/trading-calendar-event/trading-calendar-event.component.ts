import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-trading-calendar-event',
  templateUrl: './trading-calendar-event.component.html',
  styleUrls: ['./trading-calendar-event.component.scss']
})
export class TradingCalendarEventComponent implements OnInit {

  event = {
    title: 'Germany GfK Consumer Climate',
    date: 'Feb 26, 2019.',
    priority: 'high',
    indexes: {
      actual: 10.8,
      forecast: 10.8,
      previous: 9.8,
    },
    text:
      `The Gfk German Consumer Climate Index measures the level of consumer confidence in economic activity. The data is compiled from a survey of about 2,000 consumers which asks respondents to rate the relative level of past and future economic conditions.

      A higher than expected reading should be taken as positive/bullish for the EUR, while a lower than expected reading should be taken as negative/bearish for the EUR.
    `,
  };

  constructor() { }

  ngOnInit() {
  }

}
