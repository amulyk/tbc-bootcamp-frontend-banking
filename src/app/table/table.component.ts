import { Component, OnInit, Input } from '@angular/core';
import { TableService } from '../services/table.service';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {
  items = [];
  tableHeaders = [];
  propertiesArray = [];
  sortedAsc = -1;
  sortedDesc = -1;

  @Input() set content(target: object[])  {
    this.items = target;
  }

  @Input() set headers(target: object[])  {
    this.tableHeaders = target;
  }

  @Input() set properties(target: object[])  {
    this.propertiesArray = target;
  }

  constructor(private tableService: TableService) { }

  ngOnInit() {
  }

  ascSort(n) {
    this.items = this.tableService.ascSort(n, this.items);

    this.sortedDesc = -1;
    this.sortedAsc = n;
  }

  descSort(n) {
    this.items = this.tableService.descSort(n, this.items);

    this.sortedAsc = -1;
    this.sortedDesc = n;
  }

}
