import { Component, OnInit, ElementRef, HostListener } from '@angular/core';
import { LoginService } from '../login.service';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';


interface IBase {
  image: string;
  title: string;
  created: number;
  customerId: string;
  updated: string;
  message: string;
  objectId: string;
  ownerId: string;
  ___class: string;
}

@Component({
  selector: 'app-notification-dropdown',
  templateUrl: './notification-dropdown.component.html',
  styleUrls: ['./notification-dropdown.component.scss']
})
export class NotificationDropdownComponent implements OnInit {
  show = false;
  answerData = true;
  notificationsBase;
  userId = this.loginService.authenticatedUserId;
  asd$;
  time;
  scroll = false;
  sliceLimit = 2;
  count = 0;

  @HostListener('document:click', ['$event'])
  clickout(event) {
    // ignore show and close
    if (!this.eRef.nativeElement.contains(event.target) && !event.target.classList.contains('ignoreClick')) {
      this.show = false;
    }
  }

  constructor(
    private loginService: LoginService,
    private http: HttpClient,
    private eRef: ElementRef) { }

  ngOnInit() {
    // this.getNotificationData();
    this.asd$ = this.getNotificationData();
  }

  showList() {
    if (this.show) {
      this.show = false;
      clearTimeout(this.time);
    } else {
      this.show = true;
      this.time = setTimeout(() => this.show = false, 15000);
    }
  }
  getNotificationData() {
    const url = `https://api.backendless.com/B093B431-BA40-BFB4-FF22-46050E448800/A9B429C8-C84C-D894-FF06-E8783F872000/data/notifications`;
    return this.http
      .get(url)
      .pipe(map((employees: IBase[]) => {
        return employees.map(tmp => {
          if (tmp.customerId === this.userId) {
            this.answerData = true;
            this.count++;
            return {
              image: tmp.image,
              title: tmp.title,
              created: tmp.created,
              customerId: tmp.customerId,
              updated: tmp.updated,
              message: tmp.message,
              objectId: tmp.objectId,
              ownerId: tmp.ownerId,
              ___class: tmp.___class
            };
          } else {
            this.answerData = false;
          }
        });
      }));

  }
  showAll() {
    this.scroll = !this.scroll;
    if (this.scroll) {
      this.sliceLimit = this.count;
    } else {
      this.sliceLimit = 2;
    }
  }
}


