import { Component, OnInit } from '@angular/core';
import { CardService } from '../services/card.service';

@Component({
  selector: 'app-accounts-corporate',
  templateUrl: './accounts-corporate.component.html',
  styleUrls: ['./accounts-corporate.component.scss']
})
export class AccountsCorporateComponent implements OnInit {
  cards;
  credits;
  constructor(
    private cardservice: CardService,
  ) { }

  ngOnInit() {
    this.cardservice.getDeposits().subscribe(value => {
      this.cards = value;
      const total = this.cards.reduce((sum, card) => sum + card.amount, 0);
      this.cards = this.cards.map(card => {
        return { ...card, share: Math.round(100 * card.amount / total) };
      });
    });
    this.cardservice.getCredits().subscribe(value => {
      this.credits = value;
      const total = this.credits.reduce((sum, card) => sum + card.amount, 0);
      this.credits = this.credits.map(card => {
        return { ...card, share: Math.round(100 * card.amount / total) };
      });
    });

  }

}
