import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { forkJoin } from 'rxjs';
import { CardService } from '../services/card.service';

@Component({
  selector: 'app-cards',
  templateUrl: './cards.component.html',
  styleUrls: ['./cards.component.scss']
})
export class CardsComponent implements OnInit {
  type: 'CARD' | 'DEPOSIT' | 'MORTGAGE';
  constructor(
    private route: ActivatedRoute,
    private cardService: CardService
  ) { }

  account;

  ngOnInit() {
    this.route.params.subscribe(param => {
      const id = param.id;
      const allAccounts = forkJoin([
        this.cardService.getCards(),
        this.cardService.getCredits(),
        this.cardService.getDeposits()
      ])
        .subscribe((cards: any) => {
          const card = cards.flat().find((item) => item.objectId === id);
          this.account = card;
          switch (card.___class) {
            case 'cards':
              this.type = 'CARD';
              break;
            case 'deposits':
              this.type = 'DEPOSIT';
              break;
            case 'credits':
              this.type = 'MORTGAGE';
              break;
          }
        });
    });

  }

}
