import { Component, ViewChild, ElementRef, Input, OnChanges } from '@angular/core';

interface Ioption {
  width: number;
  height: number;
  points: number;
  color: string;
  data?: {}[];
}

@Component({
  selector: 'app-line-chart',
  template: '<canvas #canvas></canvas>'
})
export class LineChartComponent implements OnChanges {

  @ViewChild('canvas', { static: true }) public canvas: ElementRef;
  @Input() options: Ioption;

  constructor() { }

  private cx: CanvasRenderingContext2D;

  public ngOnChanges() {
    if (!this.options) { return; }

    const canvasEl: HTMLCanvasElement = this.canvas.nativeElement;

    this.options.points = this.options.data ? this.options.data.length : this.options.points;

    canvasEl.width = this.options.width;
    canvasEl.height = this.options.height;

    this.cx = canvasEl.getContext('2d');

    const points = [];

    if (this.options.data) {
      this.options.data.push(this.options.data[this.options.data.length - 1]);
    }

    for (let i = 0; i <= this.options.points; i++) {
      let y = Math.floor(Math.random() * canvasEl.height / 2) + 1;
      if (this.options.data) {
        y = Object.values(this.options.data[i])[0] as number;
        y = canvasEl.height / y + canvasEl.height / 4;
      }

      points.push({
        x: i * canvasEl.width / this.options.points,
        y
      });
    }

    const add = 3;

    // Create gradient
    const grd = this.cx.createLinearGradient(canvasEl.width, 0, canvasEl.width, canvasEl.height);

    // Add colors
    grd.addColorStop(0.000, 'rgba(' + this.hexToRGB(this.options.color) + ', 0.15)');
    grd.addColorStop(1.000, 'rgba(' + this.hexToRGB(this.options.color) + ', 0.02)');

    // Fill with gradient
    this.cx.fillStyle = grd;

    this.cx.lineWidth = 1.5;
    this.cx.strokeStyle = this.options.color;
    this.cx.beginPath();
    this.cx.moveTo(points[0].x, points[0].y);
    for (let i = 1; i < points.length - 1; i++) {
      const startY = points[i].y < points[i + 1].y ? points[i].y - add : points[i].y + add;
      const endY = points[i].y < points[i + 1].y ? points[i + 1].y + add : points[i + 1].y - add;
      this.cx.bezierCurveTo(points[i].x + add * 2.5, startY, points[i + 1].x - add * 2.5, endY, points[i + 1].x, points[i + 1].y);
    }
    this.cx.lineTo(points[points.length - 1].x + 2, points[points.length - 1].y + 2);
    this.cx.lineTo(canvasEl.width + 2, canvasEl.height + 2);
    this.cx.lineTo(-2, canvasEl.height + 2);
    this.cx.lineTo(points[0].x - 2, points[0].y + 2);
    this.cx.fill();
    this.cx.stroke();
  }

  hexToRGB(h) {
    let r: any;
    let g: any;
    let b: any;
    r = 0, g = 0, b = 0;

    if (h.length === 4) {
      r = '0x' + h[1] + h[1];
      g = '0x' + h[2] + h[2];
      b = '0x' + h[3] + h[3];

    } else if (h.length === 7) {
      r = '0x' + h[1] + h[2];
      g = '0x' + h[3] + h[4];
      b = '0x' + h[5] + h[6];
    }

    return +r + ',' + +g + ',' + +b;
  }

}
