export const theme = {
    CARD: {
        'payment-card': {
            background: '#4D7CFE'
        },
        headerIcon: {
            'background-image': 'url(/tbc-bootcamp-frontend-banking/assets/cards/Visa.svg)',
            'background-position': 'center',
            'background-repeat': 'no-repeat',
        },
        cardIcon: {
            visa: {
                'background-image': 'url(/tbc-bootcamp-frontend-banking/assets/cards/Visa-white.svg)',
                'background-position': 'center',
                'background-repeat': 'no-repeat',
            },
            mastercard: {
                'background-image': 'url(/tbc-bootcamp-frontend-banking/assets/cards/mastercard.svg)',
                'background-position': 'center',
                'background-repeat': 'no-repeat',
            }
        }
    },
    DEPOSIT: {
        'payment-card': {
            background: '#FFAB2B'
        },
        cardIcon: {
            visa: {
                'background-image': 'url(/tbc-bootcamp-frontend-banking/assets/cards/Visa-white.svg)',
                'background-position': 'center',
                'background-repeat': 'no-repeat',
            },
            mastercard: {
                'background-image': 'url(/tbc-bootcamp-frontend-banking/assets/cards/mastercard.svg)',
                'background-position': 'center',
                'background-repeat': 'no-repeat',
            }
        }
    },
    MORTGAGE: {
        'payment-card': {
            background: '#6DD230'
        },
        cardIcon: {
            visa: {
                'background-image': 'url(/tbc-bootcamp-frontend-banking/assets/cards/Visa-white.svg)',
                'background-position': 'center',
                'background-repeat': 'no-repeat',
            },
            mastercard: {
                'background-image': 'url(/tbc-bootcamp-frontend-banking/assets/cards/mastercard.svg)',
                'background-position': 'center',
                'background-repeat': 'no-repeat',
            }
        }
    },
};
