import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CardService } from '../services/card.service';
import { theme } from './theme';

const ddmmyy = (d) => new Date(d).toLocaleDateString('uk');
const mmyy = (d) => `${new Date(d.expirationDate).getMonth() + 1} / ${new Date(d.expirationDate).getFullYear().toString().slice(2)}`;
const amount = (n) => `${n} USD`;
const rate = (n) => `${n}%`;

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {
  @Input() type: 'CARD' | 'DEPOSIT' | 'MORTGAGE';
  @Input() account;
  theme = theme;

  content = {
    CARD: {
      header: (account) => this.cardService.formatCardNumber(account.cardNumber).hidden,
      paymentCard: {
        top: { value: (data) => this.cardService.formatCardNumber(data.cardNumber).spaced, label: 'Card number' },
        left: { value: (data) => data.cardHolder, label: 'Cardholder' },
        right: { value: mmyy, label: 'Valid' }
      },
      table: [
        { key: 'name', label: 'Card name', type: 'string' },
        { key: 'accountNumber', label: 'Account number', type: 'string' },
        { key: 'cardNumber', label: 'Card number', type: 'string', format: (n) => this.cardService.formatCardNumber(n).hidden },
        { key: 'cardHolder', label: 'Cardholder', type: 'string' },
        { key: 'expirationDate', label: 'Expiration date', type: 'string', format: ddmmyy },
        { key: 'amount', label: 'Available amount', type: 'string', format: amount },
        { key: 'additionalSecurity', label: '3D Security', type: 'string', toggle: true },
      ]
    },
    DEPOSIT: {
      header: (account) => account.name,
      paymentCard: {
        top: { value: (data) => `${data.rate}% ${data.name}`, label: 'Type' },
        left: { value: mmyy, label: 'Valid' },
      },
      table: [
        { key: 'name', label: 'Name', type: 'string' },
        { key: 'accountNumber', label: 'Account number', type: 'string' },
        { key: 'rate', label: 'Rate', type: 'string', format: rate },
        { key: 'startDate', label: 'Start date', type: 'string', format: ddmmyy },
        { key: 'expirationDate', label: 'Expiration date', type: 'string', format: ddmmyy },
        { key: 'amount', label: 'Available amount', type: 'string', format: amount },
        { key: 'accuredInterest', label: 'Accured intetest', type: 'string', format: amount },
      ]
    },
    MORTGAGE: {
      header: (account) => account.name,
      paymentCard: {
        top: { value: (data) => `${data.rate}% ${data.name}`, label: 'Type' },
        left: { value: mmyy, label: 'Until' },
      },
      table: [
        { key: 'name', label: 'Name', type: 'string' },
        { key: 'accountNumber', label: 'Account number', type: 'string' },
        { key: 'rate', label: 'Rate', type: 'string', format: rate },
        { key: 'startDate', label: 'Start date', type: 'string', format: ddmmyy },
        { key: 'expirationDate', label: 'Expiration date', type: 'string', format: ddmmyy },
        { key: 'amount', label: 'Starting amount', type: 'string', format: amount },
        { key: 'paid', label: 'Paid amount', type: 'string', format: amount },
      ]
    },
  };

  constructor(
    private cardService: CardService,
    private router: Router
  ) { }


  ngOnInit() {
  }

  delete() {
    if (confirm("Are you sure?")) {
      const event = new CustomEvent('deleteCard', { 'detail': this.account });
      this.cardService.deleteAccount(this.account, this.type)
        .subscribe(r => {
          document.dispatchEvent(event);
         })
        .add(() => {
          this.router.navigate(['/account']);
        });
    }
  }

  changeSecurity(account) {
    account.additionalSecurity = !account.additionalSecurity;

    this.cardService.updateAccount(account, this.type)
      .subscribe(r => { });
  }

}
