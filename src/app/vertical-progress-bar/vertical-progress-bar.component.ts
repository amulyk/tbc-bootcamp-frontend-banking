import { Component, OnInit, OnChanges, Input } from '@angular/core';
import { trigger, state, style, animate, transition } from '@angular/animations';

@Component({
  selector: 'app-vertical-progress-bar',
  templateUrl: './vertical-progress-bar.component.html',
  styleUrls: ['./vertical-progress-bar.component.scss'],
  animations: [
    trigger('load', [
      state('start', style({
        height: '{{endPoint}}%',
        bottom: '{{bottom}}%',
      }), { params: { endPoint: '0%', bottom: '0%' } }),
      state('change', style({
        height: '{{endPoint}}%',
        bottom: '{{bottom}}%',
      }), { params: { endPoint: '0%', bottom: '0%' } }),
      transition('* <=> *', animate('1s'))
    ])
  ]
})
export class VerticalProgressBarComponent implements OnInit, OnChanges {
  @Input() endPoint: number;
  @Input() color: string;
  changed: boolean;

  constructor() {
  }

  ngOnInit() {
    this.changed = false;
  }

  ngOnChanges() {
    this.changed = !this.changed;
  }
}
