import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { LoginService } from '../login.service';
import { IResponse } from '../login.interface';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  user;
  authError = false;
  constructor(
    private formBuilder: FormBuilder,
    private loginService: LoginService,
    private router: Router,
  ) {
    this.user = this.loginService.user;
  }

  ngOnInit() {
  }

  onSubmit(value) {
    this.loginService.onSubmit(value).subscribe((data: IResponse | any) => {
      if (data) {
        if (data.code === 3003) {
          this.authError = true;
          return;
        }
        this.loginService.finishLogin(data).subscribe(() => {
          this.router.navigate(['/dashboard']);
        });
      }
    });
  }


  get login() {
    return this.loginService.login;
  }

  get password() {
    return this.loginService.password;
  }

  get isAuth() {
    return this.loginService.auth;
  }

}
