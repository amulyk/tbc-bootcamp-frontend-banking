import { Component, OnInit } from '@angular/core';
import { NguCarouselConfig } from '@ngu/carousel';
import { NewsListService } from '../news-list.service';
@Component({
 selector: 'app-news-slider',
 templateUrl: './news-slider.component.html',
 styleUrls: ['./news-slider.component.scss'],
})
export class NewsSliderComponent implements OnInit {
 imgags = [
   'assets/bg.jpg',
   'assets/car.png',
   'assets/canberra.jpg',
   'assets/holi.jpg'
 ];
 numStart = 5;
 public carouselTileItems: Array<any> = [];
 constructor(private newsListService: NewsListService) { }
 ngOnInit() {
   this.carouselTileItems.push(this.newsListService.getData(this.numStart)
     .subscribe(value => {
       this.carouselTileItems = value;
     }));
 }
 public carouselTile: NguCarouselConfig = {
   grid: { xs: 4, sm: 4, md: 4, lg: 4, all: 0 },
   speed: 250,
   point: {
     visible: true
   },
   load: 2,
   velocity: 0,
   touch: true,
   easing: 'cubic-bezier(0, 0, 0.2, 1)'
 };
}