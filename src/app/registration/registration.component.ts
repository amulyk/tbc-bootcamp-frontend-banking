import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { RegisterService } from '../services/register.service';
import { StepperComponent } from './stepper/stepper.component';
import { Router } from '@angular/router';
import { Subject, of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { LoginService } from '../login.service';
import { IResponse } from '../login.interface';

interface ICreateUserResponse {
  code: number;
  message: string;
  errorData: {
    [key: string]: string
  };
}

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {

  mainForm: FormGroup;

  registrationFormStep1: FormGroup;
  registrationFormStep2: FormGroup;
  registrationFormStep3: FormGroup;

  // pass this property to stepper as an argument to notify when registration successed or force to go any step
  complete: Subject<boolean> = new Subject();
  goToStep: Subject<number> = new Subject();

  cardType = 'other';
  accNumberTaken = false;
  phoneTaken = false;
  primaryCardTaken = false;

  createdUserObjectId;
  // pass this error to stepper to notify whene arror occured and dont let user go to last step
  registerError;
  sameAccauntNumberError;
  constructor(
    public formBuilder: FormBuilder,
    private registerService: RegisterService,
    private router: Router,
    private loginService: LoginService
  ) {
    this.registrationFormStep1 = this.formBuilder.group({
      email: ['', [
        Validators.required,
        Validators.email
      ]],
      password: ['', [Validators.required]]
    });

    this.registrationFormStep2 = this.formBuilder.group({
      accNumber: ['', [Validators.required]],
      address: ['', Validators.required],
      city: ['', Validators.required],
      zip: ['', [Validators.required, Validators.pattern(/^3([0-9]{4})$/)]],
      state: ['', Validators.required],
      gender: ['', Validators.required],
    });

    this.registrationFormStep3 = this.formBuilder.group({
      plan: ['small business', Validators.required],
      primaryAccount: ['', [Validators.required, Validators.pattern(/^([0-9]{4}[0-9]{4}[0-9]{4}[0-9]{4})$/)]],
      phone: ['', [Validators.required, Validators.pattern(/^([0-9]{7})$/)]],
      license: [false, [Validators.required]],
      newsSubscribe: [false, [Validators.required]]
    });

    this.mainForm = this.formBuilder.group({
      registrationFormStep1: this.registrationFormStep1,
      registrationFormStep2: this.registrationFormStep2,
      registrationFormStep3: this.registrationFormStep3,
    });

    // remove server errors if we change inputs
    this.bindServerEroorCleaner();
  }

  bindServerEroorCleaner() {
    this.accNumber.valueChanges.subscribe(() => {

      if (this.accNumberTaken) {
        this.accNumberTaken = undefined;
      }

    });
    this.phone.valueChanges.subscribe(() => {

      if (this.phoneTaken) {
        this.phoneTaken = undefined;
      }

    });
    this.primaryAccount.valueChanges.subscribe(() => {

      if (this.primaryCardTaken) {
        this.primaryCardTaken = undefined;
      }

    });
  }

  ngOnInit() {
  }

  createUser(formData) {
    if (!this.registrationFormStep1.invalid) {
      const {
        registrationFormStep1: {
          email: login,
          password: password,
        },
      } = formData;

      return this.registerService.createUser(formData).pipe(catchError(error => of(this.registerError = error)), tap(async (response) => {

        const loginResponse = await this.loginService.onSubmit({ login, password })
          .pipe(catchError(error => of(this.registerError = error))).toPromise() as IResponse;

        if (response) {
          this.createdUserObjectId = response.objectId;
        }
      }));
    } else {
      // this.makeFirstStepDirty();
    }

  }

  submit(formData) {
    if (!this.mainForm.invalid && (this.license.value && this.newsSubscribe.value)) {
      const {
        registrationFormStep1: {
          email: login,
          password: password,
        },
      } = formData;

      try {

        this.loginService.onSubmit({ login, password })
          .pipe(catchError(error => of(this.registerError = error)))
          .subscribe(async (loginResponse: IResponse) => {

            const createdCustomer = await this.registerService.createCustomer(loginResponse.objectId, loginResponse['user-token'], formData)
              .toPromise() as ICreateUserResponse;

            if (createdCustomer.code === 1155) {// dublicate value error
              if (createdCustomer.errorData.columnName === 'phone') {
                this.phoneTaken = true;
                return;
              } else if (createdCustomer.errorData.columnName === 'accountNumber') {
                this.accNumberTaken = true;
                this.goToStep.next(1);
                return;
              } else if (createdCustomer.errorData.columnName === 'primaryCard') {
                this.primaryCardTaken = true;
                return;
              }

            }
            this.complete.next(true);

          });
      } catch (error) {
        throw error;
      }
    } else {
      this.makeLatsStepDirty();
    }
  }

  goLogin() {
    this.router.navigate(['/login']);
  }

  checkCard() {
    const cardNumber = this.primaryAccount.value[0];
    if (cardNumber === '5') {
      this.cardType = 'master';
    } else if (cardNumber === '4') {
      this.cardType = 'visa';
    } else {
      this.cardType = 'other';
    }
  }

  makeLatsStepDirty() {
    const controls = this.registrationFormStep3.controls;
    for (const key in controls) {
      if (controls[key]) {
        controls[key].markAsDirty();
      }
    }
  }

  makeFirstStepDirty() {
    const controls = this.registrationFormStep1.controls;
    for (const key in controls) {
      if (controls[key]) {
        controls[key].markAsDirty();
      }
    }
  }

  get accNumber() {
    return this.registrationFormStep2.get('accNumber') as FormControl;
  }
  get email() {
    return this.registrationFormStep1.get('email') as FormControl;
  }
  get password() {
    return this.registrationFormStep1.get('password') as FormControl;
  }
  get address() {
    return this.registrationFormStep2.get('address') as FormControl;
  }
  get city() {
    return this.registrationFormStep2.get('city') as FormControl;
  }
  get zip() {
    return this.registrationFormStep2.get('zip') as FormControl;
  }
  get state() {
    return this.registrationFormStep2.get('state') as FormControl;
  }
  get plan() {
    return this.registrationFormStep3.get('plan') as FormControl;
  }
  get primaryAccount() {
    return this.registrationFormStep3.get('primaryAccount') as FormControl;
  }
  get phone() {
    return this.registrationFormStep3.get('phone') as FormControl;
  }
  get license() {
    return this.registrationFormStep3.get('license') as FormControl;
  }
  get newsSubscribe() {
    return this.registrationFormStep3.get('newsSubscribe') as FormControl;
  }
  get gender() {
    return this.registrationFormStep2.get('gender') as FormControl;
  }

}
