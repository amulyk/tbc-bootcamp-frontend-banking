import { NgModule } from '@angular/core';

import { RegistrationComponent } from './registration.component';
import { StepperComponent } from './stepper/stepper.component';
import { StepComponent } from './stepper/step/step.component';
import { StepperNextDirective, StepperPreviousDirective } from './stepper/buttons.directive';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { RegisterService } from '../services/register.service';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { CheckboxComponent } from '../checkbox/checkbox.component';
import { RadioButtonComponent } from '../radio-button/radio-button.component';

@NgModule({
    declarations: [
        RegistrationComponent,
        StepperComponent,
        StepComponent,
        StepperNextDirective,
        StepperPreviousDirective,
        CheckboxComponent,
        RadioButtonComponent
    ],
    exports: [
        RegistrationComponent,
        CheckboxComponent,
        RadioButtonComponent
    ],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        HttpClientModule,
        BrowserAnimationsModule,
        FormsModule
    ],
    providers: [
        RegisterService
    ]
})
export class RegistartionModule { }
