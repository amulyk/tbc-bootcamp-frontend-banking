import { Component, OnInit, ViewChild, TemplateRef, AfterContentInit, Input } from '@angular/core';

@Component({
  selector: 'app-step',
  templateUrl: './step.component.html'
})
export class StepComponent {

  @ViewChild(TemplateRef, { static: true }) content: TemplateRef<any>;

  @Input() formGroup;
}
