import { Component, OnInit, ContentChildren, QueryList, Input, AfterContentInit, ComponentFactoryResolver, ViewChild } from '@angular/core';
import { StepComponent } from './step/step.component';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-stepper',
  templateUrl: './stepper.component.html',
  styleUrls: ['./stepper.component.scss']
})
export class StepperComponent implements OnInit, AfterContentInit {


  @ContentChildren(StepComponent) steps: QueryList<StepComponent>;

  selectedIndex = 0;
  @Input() complete: Subject<boolean>;
  @Input() forceGo: Subject<number>;
  count;

  @Input() error;

  ngAfterContentInit() {
    this.count = this.steps.length;
    if (this.forceGo) {
      this.forceGo.subscribe((pageIndex) => {
        this.selectedIndex = pageIndex;
      });
    }
    this.complete.subscribe((done) => {
      if (done) {
        this.selectedIndex = this.steps.toArray().length - 1;
      }
    });
  }
  ngOnInit() {
  }

  next() {
    if (!this.steps.toArray()[this.selectedIndex].formGroup.invalid) {
      this.selectedIndex += 1;
      if (this.selectedIndex >= this.steps.length) {
        this.selectedIndex -= 1;
      }
    } else {
      const controls = this.steps.toArray()[this.selectedIndex].formGroup.controls;
      for (const key in controls) {
        if (controls[key]) {
          controls[key].markAsDirty();
        }
      }
    }
  }
  previous() {
    this.selectedIndex -= 1;
    if (this.selectedIndex <= 0) {
      this.selectedIndex = 0;
    }
  }

}
