import { Directive, HostListener, Input } from '@angular/core';
import { StepperComponent } from './stepper.component';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';

@Directive({
    selector: 'button[appStepperNext]',
})
export class StepperNextDirective {
    constructor(public stepper: StepperComponent) { }

    @Input() observable;
    error;

    @HostListener('click')
    _handleClick() {
        if (this.observable) {
            this.observable.pipe(catchError(err => of(this.error = err))).subscribe((data) => {
                if (!data.error) {
                    this.stepper.next();
                }
            });
        } else {
            this.stepper.next();
        }
    }
}

@Directive({
    selector: 'button[appStepperPrevious]',
})
export class StepperPreviousDirective {
    constructor(public stepper: StepperComponent) { }

    @HostListener('click')
    _handleClick() {
        this.stepper.previous();
    }
}
