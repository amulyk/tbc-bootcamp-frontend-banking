import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { UserGuard } from './user.guard';
import { LoginService } from './login.service';
import { Observable } from 'rxjs';

interface Iuser {
  securityAnswer2: string;
  securityQuestion1: string;
  securityAnswer1: string;
  sequrityQuestion2: string;
}

@Injectable({
  providedIn: 'root'
})
export class SettingsSecuretyService {
  host = 'https://api.backendless.com/B093B431-BA40-BFB4-FF22-46050E448800/A9B429C8-C84C-D894-FF06-E8783F872000';
  userId = this.loginService.authenticatedUserId;
  constructor(
    private httpClient: HttpClient,
    private loginService: LoginService
  ) { }

  get() {
    const id = this.loginService.authenticatedUserId;
    const url = `${this.host}/data/customers/${id}`;
    return this.httpClient.get(url);
  }

  update(info: Iuser): Observable<Iuser> {
    const id = this.loginService.authenticatedUserId;
    const url = `${this.host}/data/customers/${id}`;
    return this.httpClient.put<Iuser>(url, info);
  }
}
