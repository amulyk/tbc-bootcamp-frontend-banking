import { Injectable } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Router } from '@angular/router';

import { ICustomer, IResponse } from './login.interface';
import { catchError, finalize, tap } from 'rxjs/operators';
import { of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  auth: boolean;
  user;
  response;
  authUser;
  userInfo: ICustomer;
  host;
  loginUrl;
  customersUrl;
  value;

  constructor(
    private formBuilder: FormBuilder,
    private httpClient: HttpClient,
    private route: Router
  ) {
    this.user = this.formBuilder.group({
      login: ['', Validators.email],
      password: ''
    });
    this.host = 'https://api.backendless.com/B093B431-BA40-BFB4-FF22-46050E448800/A9B429C8-C84C-D894-FF06-E8783F872000';
    this.loginUrl = `${this.host}/users/login`;
    this.customersUrl = `${this.host}/data/customers`;
  }

  onSubmit(value) {
    this.value = value;
    return this.httpClient.post(this.loginUrl, value).pipe(catchError(error => of(error.error)));
  }

  finishLogin(data: IResponse) {
    if (data) {
      this.response = data;
      this.authUser = { userId: data.objectId, token: data['user-token'] };
      const customerParams = new HttpParams().set('where', `userId='${data.objectId}'`);
      return this.httpClient.get(this.customersUrl, { headers: { 'user-token': data['user-token'] }, params: customerParams })
        .pipe(catchError(error => of(this.auth = false)), tap((customers: ICustomer[]) => this.setUserData(customers)));
    }
  }

  setUserData(customers: ICustomer[]) {
    for (const customer of customers) {
      if (this.authUser.userId === customer.userId) {
        this.authUser.userId = customer.objectId;
        this.userInfo = customer;
        const obj = JSON.stringify(this.authUser);
        localStorage.setItem('User', obj);
        this.auth = true;
      }
    }
  }


  set setUser(user) {
    this.authUser = user;
  }

  get isAuth() {
    return this.auth;
  }

  get login() {
    return this.user.get('login') as FormControl;
  }

  get password() {
    return this.user.get('password') as FormControl;
  }

  get authenticatedUserId() {
    return this.authUser.userId;
  }

  get authenticatedUserToken() {
    return this.authUser.token;
  }

  get readUserInfo() {
    return this.userInfo;
  }

  get customer() {
    const url = `${this.customersUrl}/${this.authUser.userId}`;
    return this.httpClient.get(url);
  }

  get gender() {
    return this.readUserInfo.sex;
  }

  logout() {
    const logoutUrl = `${this.host}/users/logout`;
    this.httpClient.get(logoutUrl).subscribe({
      complete: () => {
        localStorage.removeItem('User');
        this.auth = false;
      }
    });
  }
}
