import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard-empty-account',
  templateUrl: './dashboard-empty-account.component.html',
  styleUrls: ['./dashboard-empty-account.component.scss']
})
export class DashboardEmptyAccountComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
