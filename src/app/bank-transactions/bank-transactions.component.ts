import { Component, OnInit, ViewEncapsulation, Input } from '@angular/core';
import { BankTransactionsService } from '../services/bank-transactions.service';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { DatePipe } from '@angular/common';
import { FormBuilder, FormControl } from '@angular/forms';
import { elementAt } from 'rxjs/operators';

@Component({
  selector: 'app-bank-transactions',
  templateUrl: './bank-transactions.component.html',
  styleUrls: ['./bank-transactions.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [DatePipe],
  animations: [
    trigger('dialog', [
      state('on', style({
        transform: 'scale(1)',
        opacity: 1,
        'z-index': 99,
      })),
      state('off', style({
        transform: 'scale(0.8)',
        opacity: 0,
        'z-index': -1,
      })),
      transition('off <=> on', animate(150)),
    ]),
    trigger('dialog2', [
      state('on', style({
        display: 'block',
      })),
      state('off', style({
        display: 'none',
      })),
      transition('off <=> on', animate(10)),
    ]),
    trigger('dialog3', [
      state('on', style({
        filter: 'blur(2px)',
      })),
      state('off', style({
        filter: 'none',
      })),
      transition('off <=> on', animate(10)),
    ]),
  ]
})
export class BankTransactionsComponent implements OnInit {
  type = [{value: 'ALL'}];
  tempType;
  form;
  trArray;
  actualData = [{
    title: '',
    type: '',
    amount: '',
    displayDate: '',
    created: ''
  }];
  currentTransaction;
  filterSearchData;
  filterDateData;
  filterTypeData;
  status = false;
  @Input() smallMode: boolean;
  @Input() title: string;

  cardNumber;
  cardsArr = [];
  creditsArr = [];

  constructor(
    private bankTransactionsService: BankTransactionsService,
    private datePipe: DatePipe,
    private formBuilder: FormBuilder
  ) {
    this.form = formBuilder.group({
      item: ['ALL']
    });
  }

  ngOnInit() {
    this.bankTransactionsService.getTransactions().subscribe(v => {
      this.trArray = v;
      const finalArray = [];
      for (const tr of this.trArray) {
        if (tr.receiverId === this.userId || tr.senderId === this.userId) {
          finalArray.push(tr);
        }
        this.tempType = {};
        this.tempType.value = tr.type;
        let bool = true;
        for (const type1 of this.type) {
          if (type1.value === tr.type) {
            bool = false;
          }
        }
        if (bool) {
          this.type.push(this.tempType);
        }

        this.bankTransactionsService.getCards('cards', tr.cardId)
        .subscribe(
          card => {
            this.cardsArr.push(card);
          }
        );

        this.bankTransactionsService.getCards('credits', tr.cardId)
        .subscribe(
          card => {
            this.creditsArr.push(card);
          }
        );
      }
      this.actualData = this.trArray = finalArray;
      for (const data of this.actualData) {
        const currDate = new Date(data.created);
        const day = currDate.toLocaleString('default', {day: 'numeric'});
        const month = currDate.toLocaleString('default', {month: 'short'});
        const hour = currDate.toLocaleString('default', {hour: 'numeric', minute: '2-digit'});
        data.displayDate = `${day} ${month} ${hour}`;
      }
    });
  }

  filterAll(search, type, date) {
    if (typeof search !== 'undefined') {
      this.filterSearch(search, 1);
    }

    if (typeof type !== 'undefined') {
      this.filterType(type, 1);
    }

    if (typeof date !== 'undefined') {
      this.filterDate(date, 1);
    }
  }

  filterDate(date, b) {
    let tempVariable = this.trArray;
    if (b === 1) {
      tempVariable = this.actualData;
    }
    this.filterDateData = date;
    if (typeof this.filterDateData !== 'undefined') {
      const start = this.datePipe.transform(date.startDate._d, 'dd/M/yyyy');
      const end = this.datePipe.transform(date.endDate._d, 'dd/M/yyyy');
      this.actualData = tempVariable.filter(it => {
        const currDate = new Date();
        currDate.setTime(it.created);
        const a =  this.datePipe.transform(currDate, 'dd/M/yyyy');
        return a >= start && a <= end ?  true : false;
      });
    }
  }

  filterSearch(val, a) {
    let tempVariable = this.trArray;
    if (a === 1) {
      tempVariable = this.actualData;
    }
    this.filterSearchData = val;
    if (typeof val !== 'undefined') {
      if (val.target.value.length > 0) {
        const input = val.target.value.toLowerCase();
        this.actualData = tempVariable.filter(it => {
          const words1 = it.title.split(' ');
          const words2 = it.type.split(' ');
          const words = words1.length > words2.length ? words1 : words2;
          for (let i = 0; i < words.length; i++) {
            if ((words1[i].toLowerCase().charAt(0) === input.charAt(0) || words2[i].toLowerCase().charAt(0) === input.charAt(0)) &&
            (it.title.toLowerCase().includes(input) || it.type.toLowerCase().includes(input))) {
              return true;
            }
          }
          return false;
        });
      } else {
        this.actualData = tempVariable;
      }
    }
  }

  filterType(type, a) {
    let tempVariable = this.trArray;
    if (a === 1) {
      tempVariable = this.actualData;
    }
    this.filterTypeData = type;
    if (typeof type !== 'undefined') {
      if (type === 'ALL') {
        this.actualData = tempVariable;
      } else {
        this.actualData = tempVariable.filter(it => it.type.toLowerCase().includes(type));
      }
    }
  }

  showDetails(objectId, ownerId) {
    this.status = true;
    for (const transaction of this.trArray) {
      if (transaction.objectId === objectId && transaction.ownerId === ownerId) {
        this.currentTransaction = transaction;
        for (const card1 of this.cardsArr) {
          if (card1.length) {
            const card = card1[0];
            if (this.currentTransaction.cardId === card.objectId) {
              this.cardNumber = 'xxxx ' + card.cardNumber.toString().slice(-4);
            }
          }
        }

        for (const card1 of this.creditsArr) {
          if (card1.length) {
            const card = card1[0];
            if (this.currentTransaction.cardId === card.objectId) {
              this.cardNumber = 'xxxx ' + card.cardNumber.toString().slice(-4);
            }
          }
        }
      }
    }
  }

  hideDetails() {
    this.status = false;
  }

  get userId() {
    return this.bankTransactionsService.userId;
  }

  get item() {
    return this.form.get('item') as FormControl;
  }

}
