export interface ITransaction {
    receiverId: string;
    senderId: string;
    ownerId: string;
    image: string;
    title: string;
    type: string;
    updated: number;
    created: number;
    cardId: string;
    amount: number;
    objectId: string;
}
