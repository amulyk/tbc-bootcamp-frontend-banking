export interface IDeposit {
    ownerId?: string;
    updated?: string;
    startDate?: string;
    objectId?: string;
    cardNumber?: number;
    expirationDate?: string;
    rate?: number;
    accountNumber?: string;
    customerId?: string;
    created?: number;
    accuredInterest?: number;
    amount?: number;
    name?: string;
}
