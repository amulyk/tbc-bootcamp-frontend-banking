export interface ICredit {
  paid: number;
  amount: number;
  name: string;
  cardNumber: number;
  rate: number;
  customerId: string;
  startDate: string;
  accountNumber: string;
  expirationDate: string;
}
