export interface ICard {
    accountNumber?: string;
    additionalSecurity?: boolean;
    amount?: number;
    cardHolder?: string;
    cardNumber?: number;
    created?: number;
    customerId?: string;
    expirationDate?: number;
    name?: string;
    objectId?: string;
    ownerId?: string;
    updated?: number;
    type?: string;
    cardTypes?: {
        mini: string;
        full: string;
        original: string;
    };
}
