import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
} from '@angular/common/http';
import {Observable, pipe} from 'rxjs';
import {tap} from 'rxjs/operators';
import {LoginService} from './login.service';
import {IResponse} from './login.interface';
import {Router} from '@angular/router';
import { finalize } from 'rxjs/operators';
import { LoaderService } from '../app/services/loader.service';

@Injectable()
export class Interceptor implements HttpInterceptor {
  constructor(private loginService: LoginService, private router: Router, public loaderService: LoaderService) {}
  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    if (localStorage.getItem('User') && request.url.includes('api.backendless.com')) {
      const user = JSON.parse(localStorage.getItem('User'));

      this.loaderService.show();
      if (user && user.token) {
        request = request.clone({
          setHeaders: {
            'user-token': user.token
          }
        });
      }
    }

    return next.handle(request)
      .pipe(tap(
        () => {
        },
        error => {
          if (error.error.code === 3064 || error.error.code === 1013 || error.error.code === 3002) {
            this.loginService.logout();
            this.router.navigate(['/login']);
          }
        }
      ), finalize(() => {this.loaderService.hide(); }));
  }
}
