import {Component, OnInit} from '@angular/core';
import {CardService} from '../services/card.service';

@Component({
  selector: 'app-accounts-balance',
  templateUrl: './accounts-balance.component.html',
  styleUrls: ['./accounts-balance.component.scss']
})
export class AccountsBalanceComponent implements OnInit {
  cards;

  constructor(private cardService: CardService) {
  }

  ngOnInit() {
    this.cardService.getCards().subscribe(value => this.cards = value);
  }

}
