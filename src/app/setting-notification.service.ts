import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LoginService } from './login.service';
import { map } from 'rxjs/operators';

interface Inotifiaction {
  isProductUpdatesEnabled: boolean;
  isOfferUpdatesEnabled: boolean;
  isCommentsEnabled: boolean;
  isNotificationsEnabled: boolean;
}

@Injectable({
  providedIn: 'root'
})
export class SettingNotificationService {
  private option = {
    product: false,
    offer: false,
    comments: false,
    notification: false,
  };
  private url = 'https://api.backendless.com/B093B431-BA40-BFB4-FF22-46050E448800/A9B429C8-C84C-D894-FF06-E8783F872000';

  constructor(
    private http: HttpClient,
    private loginService: LoginService
  ) { }
  getOptions() {
    const endPoint = '/data/customers/' + this.loginService.authenticatedUserId;
    return this.http.get(this.url + endPoint)
      .pipe(map((notification: Inotifiaction) => {
        return this.option = {
          product: notification.isProductUpdatesEnabled,
          offer: notification.isOfferUpdatesEnabled,
          comments: notification.isCommentsEnabled,
          notification: notification.isNotificationsEnabled,
        };
      }));
  }
  setOptions(options) {
    const endPoint = `/data/customers/${this.loginService.authenticatedUserId}`;
    this.http.put(this.url + endPoint, {
      isCommentsEnabled: options.comments,
      isNotificationsEnabled: options.notification,
      isOfferUpdatesEnabled: options.offer,
      isProductUpdatesEnabled: options.product,
    }).subscribe(value => {
      console.log(value);
    });
    this.option = { ...this.option, ...options };
  }
}
