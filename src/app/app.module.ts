import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TextareaWithEmojiComponent } from './textarea-with-emoji/textarea-with-emoji.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SettingsGeneralInformationsComponent } from './settings-general-informations/settings-general-informations.component';
import { NotificationDropdownComponent } from './notification-dropdown/notification-dropdown.component';
import { RegistartionModule } from './registration/registartion.module';
import { SettingNoteComponent } from './setting-note/setting-note.component';
// import { RadioButtonComponent } from './radio-button/radio-button.component';
import { ToggleComponent } from './toogle/toggle.component';
// import { CheckboxComponent } from './checkbox/checkbox.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { TagsComponent } from './tags/tags.component';
import { WrapperComponent } from './wrapper/wrapper.component';
import { TabsComponent } from './tabs/tabs.component';
import { TabComponent } from './tabs/tab/tab.component';
import { HeaderProfileComponent } from './header-profile/header-profile.component';
import { TagComponent } from './tag/tag.component';
import { LineChartComponent } from './componenets/line-chart/line-chart.component';
import { AccountComponent } from './account/account.component';
import { SelectFieldsComponent } from './select-fields/select-fields.component';
import { DatePickerComponent } from './date-picker/date-picker.component';
import { NgxDaterangepickerMd } from 'ngx-daterangepicker-material';
import { EmojisModule } from 'ng2-emojis';
import { CryptoCurrenciesComponent } from './crypto-currencies/crypto-currencies.component';
import { PaymentLimitsComponent} from './payment-limits/payment-limits.component';
import { CreateCardComponent } from './create-card/create-card.component';
import { BankTransactionsComponent } from './bank-transactions/bank-transactions.component';
import { Interceptor } from './interceptor.interceptor';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { LoginComponent } from './login/login.component';
import { ProgressBarComponent } from './progress-bar/progress-bar.component';
import { AccountsBalanceComponent } from './accounts-balance/accounts-balance.component';
import { NewsArticleComponent } from './news-article/news-article.component';
import { SettingsComponent } from './settings/settings.component';
import { TradingCalendarComponent } from './trading-calendar/trading-calendar.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AccountsCorporateComponent } from './accounts-corporate/accounts-corporate.component';
import { DashboardIncomeLineChartComponent } from './dashboard-income-line-chart/dashboard-income-line-chart.component';
import { ChartsModule } from 'ng2-charts';
import { DashboardCardsBarChartComponent } from './dashboard-cards-bar-chart/dashboard-cards-bar-chart.component';
import { VerticalProgressBarComponent } from './vertical-progress-bar/vertical-progress-bar.component';
import { TradingCalendarEventComponent } from './trading-calendar-event/trading-calendar-event.component';
import { StocksComponent } from './stocks/stocks.component';
import { SettingsSecurityComponent } from './settings-security/settings-security.component';
import { ErrorComponent } from './error/error.component';
import { PaginationComponent } from './pagination/pagination.component';
import { DashboardEmptyAccountComponent } from './dashboard-empty-account/dashboard-empty-account.component';
import { LoaderService } from './services/loader.service';
import { TradingCurrenciesCorporateComponent } from './trading-currencies-corporate/trading-currencies-corporate.component';
import { TableComponent } from './table/table.component';
import { NewsComponent } from './news/news.component';
import { PaymentsComponent } from './payments/payments.component';
import { InstantTransferComponent } from './payments/instant-transfer/instant-transfer.component';
import { BankTransfterComponent } from './payments/bank-transfer/bank-transfter.component';
import { OnlinePaymentComponent } from './payments/online-payment/online-payment.component';
import { CardComponent } from './card/card.component';
import { CreditOverviewComponent } from './credit-overview/credit-overview.component';
import { CardsComponent } from './cards/cards.component';
import { NewsSliderComponent } from './news-slider/news-slider.component';
import { NguCarouselModule } from '@ngu/carousel';
import { NewsListComponent } from './news-list/news-list.component';

@NgModule({
  declarations: [
    AppComponent,
    TextareaWithEmojiComponent,
    NotificationDropdownComponent,
    SettingNoteComponent,
    SettingsGeneralInformationsComponent,
    ToggleComponent,
    NotificationsComponent,
    TagsComponent,
    WrapperComponent,
    TabsComponent,
    PaymentLimitsComponent,
    TabComponent,
    // CheckboxComponent,
    TagComponent,
    LineChartComponent,
    AccountComponent,
    // RadioButtonComponent,
    DatePickerComponent,
    CryptoCurrenciesComponent,
    CreateCardComponent,
    SelectFieldsComponent,
    LoginComponent,
    BankTransactionsComponent,
    HeaderProfileComponent,
    AccountsBalanceComponent,
    DashboardEmptyAccountComponent,
    HeaderProfileComponent,
    ProgressBarComponent,
    TradingCalendarComponent,
    NewsArticleComponent,
    DashboardComponent,
    AccountsCorporateComponent,
    DashboardCardsBarChartComponent,
    VerticalProgressBarComponent,
    TradingCalendarEventComponent,
    ErrorComponent,
    DashboardIncomeLineChartComponent,
    SettingsSecurityComponent,
    ErrorComponent,
    TradingCurrenciesCorporateComponent,
    TableComponent,
    NewsComponent,
    InstantTransferComponent,
    BankTransfterComponent,
    OnlinePaymentComponent,
    SettingsComponent,
    PaymentsComponent,
    PaginationComponent,
    CreditOverviewComponent,
    CardComponent,
    CardsComponent,
    SettingsComponent,
    NewsSliderComponent,
    NewsListComponent,
    StocksComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RegistartionModule,
    BrowserAnimationsModule,
    NgxDaterangepickerMd.forRoot(),
    EmojisModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    ChartsModule,
    NguCarouselModule
  ],
  providers: [
    LoaderService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: Interceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent],
  entryComponents: [ NotificationsComponent ]
})
export class AppModule { }
