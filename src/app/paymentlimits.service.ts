import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { ICustomer } from './login.interface';
import { LoginService } from './login.service';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PaymentlimitsService {
  paymentLimitsObj;
  userID;
  CardAmounts = {
    card: 0,
  };
  host = 'https://api.backendless.com/B093B431-BA40-BFB4-FF22-46050E448800/A9B429C8-C84C-D894-FF06-E8783F872000';

  constructor(
    private http: HttpClient,
    private loginService: LoginService,
  ) {
    this.userID = this.loginService.authenticatedUserId;
    this.getLimitValues();
    this.getCreditCardValues();
  }

  updateForm(obj) {
    const apiURL = `${this.host}/data/customers/${this.userID}`;
    return this.http.put(`${apiURL}`, obj, this.userID);
  }

  getLimitValues() {
    const apiURL = `${this.host}/data/customers/${this.userID}`;
    return this.http.get<ICustomer>(apiURL)
      .pipe(tap(data => {
        this.paymentLimitsObj = {
          cashWithdrawalsLimit: data.cashWithdrawalsLimit,
          bankTransactionsLimit: data.bankTransactionsLimit,
          onlinePaymentsLimit: data.onlinePaymentsLimit,
        };
      }));
  }

  get userparams() {
    return new HttpParams().set('where', `customerId='${this.loginService.authenticatedUserId}'`);
  }

  getCardValues() {
    const apiURL = `${this.host}/data/cards`;
    return this.http.get(apiURL, { params: this.userparams });
  }

  getCreditCardValues() {
    const apiURL = `${this.host}/data/credits`;
    return this.http.get(apiURL, { params: this.userparams });
  }


}
