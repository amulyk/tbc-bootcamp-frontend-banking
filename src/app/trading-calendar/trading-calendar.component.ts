import { Component, OnInit } from '@angular/core';
import {TradingCalendarService} from '../services/trading-calendar.service';
import { database } from '../services/dbCalendar';
import { trigger, state, style } from '@angular/animations';

@Component({
  selector: 'app-trading-calendar',
  templateUrl: './trading-calendar.component.html',
  styleUrls: ['./trading-calendar.component.scss'],
  animations: [
    trigger('lowMedHi', [
      state('low', style({
        backgroundColor: 'red',
        color: 'orange'
      })),
      state('med', style({
        backgroundColor: 'yellow',
        color: 'orange'
      })),
      state('hig', style({
        backgroundColor: 'green',
        color: 'orange'
      }))
    ])
  ]
})
export class TradingCalendarComponent implements OnInit {
  data = [...database];

  constructor(
    private tradingcalendarService: TradingCalendarService) { }

  ngOnInit() {
  }

  get headers() {
    return this.tradingcalendarService.headers;
  }

}
