import { Component, forwardRef, Input, OnInit } from '@angular/core';
import { ControlValueAccessor, FormArray, FormBuilder, FormControl, FormGroup, NG_VALUE_ACCESSOR } from '@angular/forms';
import { animate, state, style, transition, trigger } from '@angular/animations';

@Component({
  selector: 'app-tags',
  templateUrl: './tags.component.html',
  styleUrls: ['./tags.component.scss'],
  animations: [
    trigger('hideShow', [

      state('show', style({
        opacity: 1
      })),
      state('hide', style({
        opacity: 0
      })),
      transition(':enter, :leave', animate(0))
    ])
  ],
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => TagsComponent),
    multi: true
  }]
})
export class TagsComponent implements OnInit, ControlValueAccessor {
  show = false;
  @Input() inputTags = [];
  @Input() title: string;
  @Input() placeholder: string;
  @Input() tagsTitle: string;
  selectedTags = [];
  tagsForm: FormGroup;

  constructor(private formBuilder: FormBuilder) {
    this.tagsForm = this.formBuilder.group({
      tags: new FormArray([])
    });
  }

  private onTouch = Function;
  private onModelChange = (_: any) => { };

  ngOnInit() {
  }

  addTag(tag: HTMLInputElement) {
    (this.tagsForm.get('tags') as FormArray).value.push(new FormControl(tag));
    this.selectedTags.push(tag);
    this.onModelChange(this.selectedTags);
    this.onTouch();
  }

  removeSelectedTag(tag) {
    this.selectedTags.splice(this.selectedTags.indexOf(tag), 1);
    this.inputTags.push(tag);
    this.onModelChange(this.selectedTags);
    return this.selectedTags;
  }

  removeTagFromList(tag) {
    this.inputTags.splice(this.inputTags.indexOf(tag), 1);
    return this.selectedTags;
  }

  hideShowList() {
    this.show = !this.show;
  }

  registerOnChange(fn: any): void {
    this.onModelChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouch = fn;
  }

  setDisabledState(isDisabled: boolean): void {
  }

  writeValue(obj: any): void {
    this.selectedTags = obj;
  }

}
