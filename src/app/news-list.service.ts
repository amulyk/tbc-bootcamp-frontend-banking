import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';

interface InewsDataBase {
  status: string;
  totalResults: number;
  articles: [{
    source: {
      id: string;
      name: string;
    }
    author: string;
    title: string;
    description: string;
    url: string;
    urlToImage: string;
    publishedAt: string;
    content: string;
  }];
}

@Injectable({
  providedIn: 'root'
})
export class NewsListService {

  url = 'https://newsapi.org/v2/everything?sources=financial-post&pageSize=';
  apiKey = '&apiKey=886faf57284140dd830bf5d6ddadd32f';
  constructor(private http: HttpClient) { }

  getData(num) {
    return this.http
    .get(`${this.url}${num}${this.apiKey}`)
    .pipe(
      catchError(this.handleError),
      map((storage: InewsDataBase) => {
      return storage.articles.map( storages => {
        return {
          source: {
            id: storages.source.id,
            name: storages.source.name,
          },
          author: storages.author,
          title: storages.title,
          description: storages.description,
          url: storages.url,
          urlToImage: storages.urlToImage,
          publishedAt: storages.publishedAt,
          content: storages.content
      };
      });
    }));
  }

  handleError(error) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // client-side error
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }

    return throwError(errorMessage);
  }
}
