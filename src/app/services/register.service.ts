import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { reduce, map, catchError, filter } from 'rxjs/operators';
import { of } from 'rxjs';
import { IResponse, ICustomer } from '../login.interface';


@Injectable({
    providedIn: 'root'
})
export class RegisterService {

    baseUrl = 'https://api.backendless.com/B093B431-BA40-BFB4-FF22-46050E448800/A9B429C8-C84C-D894-FF06-E8783F872000';
    registerUrl = '/users/register';
    updateUserUrl = '/data/customers';
    constructor(
        private httpClient: HttpClient
    ) {

    }
    /**
     * Creates user
     * @param formData IRegisterForm
     * @returns `Observable` of http request
     */
    createUser(formData) {
        const {
            registrationFormStep1: {
                email: email,
                password: password,
            },
        } = formData;
        return this.httpClient.post(`${this.baseUrl}${this.registerUrl}`, {
            email,
            password,
        });
    }
    /**
     * Creates customer
     * @param userId user id after login
     * @param token user token after login
     * @returns `Observable` of http request
     */
    createCustomer(userId, token, formData) {
        const {
            registrationFormStep1: {
                email,
            },
            registrationFormStep2: {
                accNumber: accountNumber,
                address: addressLine,
                city,
                zip,
                state,
                gender: sex
            },
            registrationFormStep3: {
                plan,
                primaryAccount: primaryCard,
                phone,
            }
        } = formData;
        return this.httpClient.post(`${this.baseUrl}${this.updateUserUrl}`, {
            accountNumber,
            addressLine,
            city,
            zip,
            state,
            plan,
            primaryCard,
            phone,
            sex,
            email,
            userId
        }, {
                headers: new HttpHeaders({
                    'Content-Type': 'application/json',
                    'user-token': token
                })
            }).pipe(
                catchError(error => of(error.error))
            );
    }
    /**
     * update customer
     * @param userId user id after login
     * @param token user token after login
     * @param formData `IRegisterForm`
     * @returns `Observable` of http request
     */
    updateCustomer(userId, token, formData) {
        const {
            registrationFormStep2: {
                accNumber: accountNumber,
                address: addressLine,
                city,
                zip,
                state,
            },
            registrationFormStep3: {
                plan,
                primaryAccount: primaryCard,
                phone,
            }
        } = formData;
        return this.httpClient.put(`${this.baseUrl}${this.updateUserUrl}/${userId}`, {
            accountNumber,
            addressLine,
            city,
            zip,
            state,
            plan,
            primaryCard,
            phone
        }, {
                headers: new HttpHeaders({
                    'Content-Type': 'application/json',
                    'user-token': token
                })
            }).pipe(
                catchError(error => of(`Bad Promise: ${error}`))
            );

    }

    getAccauntByAccNumber(accNumber, token) {
        return this.httpClient.get(`${this.baseUrl}${this.updateUserUrl}`, { headers: { 'user-token': token } }).pipe(
            catchError(error => of(`Bad Promise: ${error}`)),
            filter((val: ICustomer) => {
                console.log(val);

                return accNumber === val.accountNumber;
            }),
        );
    }
}
