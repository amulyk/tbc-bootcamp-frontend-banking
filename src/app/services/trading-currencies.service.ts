import { Injectable } from '@angular/core';
import { dataBase } from './dbCurrencies';

@Injectable({
  providedIn: 'root'
})
export class TradingCurrenciesService {
  headers = ['Index', 'Month', 'Last', 'High', 'Low', 'Chg.', 'Chg.%', 'Time'];

  constructor() { }


  getCurrencies() {
    return dataBase;
  }
}
