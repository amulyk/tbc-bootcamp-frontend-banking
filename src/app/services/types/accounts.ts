export interface ICard {
    amount: number;
    cardNumber: number;
    name: string;
    customerId: string;
    expirationDate: string;
    created: string;
    objectId: string;
    ownerId: string;
    additionalSecurity: boolean;
    updated: string;
    cardHolder: string;
    accountNumber: string;
}

export interface IDeposit {
    ownerId: string;
    updated: string;
    startDate: string;
    objectId: string;
    cardNumber: number;
    expirationDate: string;
    rate: number;
    accountNumber: string;
    customerId: string;
    created: string;
    accuredInterest: number;
    amount: number;
    name: string;
}

export interface ICredit {
    paid: number;
    ownerId: string;
    amount: number;
    objectId: string;
    name: string;
    rate: number;
    customerId: string;
    startDate: string;
    accountNumber: string;
    cardNumber: number;
    created: string;
    updated: string;
    expirationDate: string;
}


