import { Injectable, Input, Output } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

interface ICryptoCurrencies {
  DISPLAY: {};
}

@Injectable({
  providedIn: 'root'
})
export class CryptoCurrenciesService {
  private cryptoArray = ['BTC', 'ETH', 'XRP', 'LTC', 'USDT', 'XLM', 'BNB', 'XMR', 'MIOTA', 'DASH', 'ZEC'];

  constructor(private http: HttpClient) { }

  getCryptoCurrencies(currency, search = null) {
    const url = 'assets/crypto-currencies/cryptocurrencies.json';
    return this.http
      .get(url)
      .pipe(map((cryptocurrencies: ICryptoCurrencies[]) => {
        return this.cryptoArray.map(item => {
          const current = cryptocurrencies[0].DISPLAY[item];
          return {
            Name: current[currency].NAME,
            Price: current[currency].PRICE,
            MarketCap: current[currency].MKTCAP,
            Volume: current[currency].VOLUMEHOURTO,
            TotalVol: current[currency].TOTALVOLUME24HTO,
            Changes24H: parseFloat(current[currency].CHANGEPCT24HOUR),
            Changes7D: parseFloat(current[currency].CHANGEPCTDAY),
            Image: current[currency].IMAGEURL,
            Symbol: current[currency].FROMSYMBOL
          };
        }).filter(item => {
          if (search) {
            return item.Name.toUpperCase().includes(search.toUpperCase());
          }
          return item;
        });
      }));
  }
}
