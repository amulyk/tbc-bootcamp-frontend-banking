import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { LoginService } from '../login.service';
import { map } from 'rxjs/operators';
import { ITransaction } from '../interfaces/transaction';

@Injectable({
  providedIn: 'root'
})
export class TransactionService {

  url = 'https://api.backendless.com/B093B431-BA40-BFB4-FF22-46050E448800/A9B429C8-C84C-D894-FF06-E8783F872000';
  constructor(
    private http: HttpClient,
    private userService: LoginService
  ) { }

  userparams(date = null) {
    const owner = `senderId='${this.userId}' or receiverId='${this.userId}'`;
    const params = new HttpParams();

    return date ? params.set('where', `(${owner}) and created>='${date}'`) : params.set('where', owner);
  }

  getTransactions() {
    const endPoint = '/data/transactions';

    return this.http.get(this.url + endPoint, { params: this.userparams() });
  }

  getUserTransactions() {
    const endPoint = '/data/transactions';
    const endDate = this.setDate(4);
    const params = this.userparams(endDate);

    return this.http.get(this.url + endPoint, { params }).pipe(map((value: []) => {
      return {
        times: this.modifyData(value, true),
        current: this.formatTime()
      };
    }));
  }

  getInOut() {
    const endPoint = '/data/transactions';
    const endDate = this.setDate(4);
    const params = this.userparams(endDate);

    return this.http.get(this.url + endPoint, { params }).pipe(map((value: []) => {
      return this.modifyData(value);
    }));
  }

  makeTransation(options) {
    const endPoint = '/data/transactions';
    const body = { ...options, senderId: this.userId };

    return this.http.post(this.url + endPoint, body);
  }

  setDate(month) {
    const currantDate = new Date();
    currantDate.setDate(1);
    currantDate.setMonth(currantDate.getMonth() - month);
    currantDate.setHours(0, 0, 0);
    currantDate.setMilliseconds(0);
    currantDate.getTime();

    return currantDate.getTime();
  }

  modifyData(data, prev = null) {
    return data.map((item: ITransaction) => {
      const time = this.formatTime(item.created, prev);
      const amount = item.receiverId === this.userId ? item.amount : -item.amount;

      const mShort = { month: 'short', };
      const mLong = { month: 'long', };
      const date  = new Date(item.created);

      const month = date.toLocaleDateString('en-US', mLong);
      const monthShort = date.toLocaleDateString('en-US', mShort);

      return { time, amount, cardId: item.cardId, created: item.created, month, monthShort };
    });
  }

  formatTime(time = null, prev?) {
    const curDate = time ? new Date(time) : new Date();
    if (prev) {
      curDate.setMonth(curDate.getMonth() - 1);
    }
    let month = (curDate.getMonth() + 1).toString();
    month = month.length === 1 ? '0' + month : month;
    const year = curDate.getFullYear().toString().slice(2);

    return month + '/' + year;
  }

  get userId() {
    return this.userService.authenticatedUserId;
  }

}
