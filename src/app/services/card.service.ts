import { ICard } from '../interfaces/card';
import { IDeposit } from './../interfaces/deposit';
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { LoginService } from '../login.service';
import { map, catchError } from 'rxjs/operators';
import { ICredit } from '../interfaces/credits';
import { of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CardService {

  url = 'https://api.backendless.com/B093B431-BA40-BFB4-FF22-46050E448800/A9B429C8-C84C-D894-FF06-E8783F872000';
  constructor(
    private http: HttpClient,
    private userService: LoginService
  ) { }

  get userparams() {
    return new HttpParams().set('where', `customerId='${this.userService.authenticatedUserId}'`);
  }

  getCards() {
    const endPoint = '/data/cards';

    return this.http.get(this.url + endPoint, { params: this.userparams }).pipe(map((value: []) => value.map((item: ICard) => {
          const type = item.cardNumber.toString()[0] === '3' ? 'visa' : 'mastercard';
          const cardTypes = this.transformCardNumber(item.cardNumber);
          return { ...item, type, ...cardTypes };
        })
        ));

  }

  getCard(id) {
    const endPoint = '/data/cards/' + id;

    return this.http.get(this.url + endPoint, { params: this.userparams });      
  }

  getDeposits() {
    const endPoint = '/data/deposits';

    return this.http.get(this.url + endPoint, { params: this.userparams }).pipe(map((value: []) => value.map((item: ICard) => {
      const type = item.cardNumber.toString()[0] === '3' ? 'visa' : 'mastercard';
      const cardTypes = this.transformCardNumber(item.cardNumber);
      return { ...item, type, ...cardTypes };
    })
    ));
  }

  getCredits() {
    const endPoint = '/data/credits';

    return this.http.get(this.url + endPoint, { params: this.userparams }).pipe(map((value: []) => value.map((item: ICard) => {
      const type = item.cardNumber.toString()[0] === '3' ? 'visa' : 'mastercard';
      const cardTypes = this.transformCardNumber(item.cardNumber);
      return { ...item, type, ...cardTypes };
    })
    ));
  }

  updateAmmount(cardId, amount) {
    const endPoint = '/data/cards/' + cardId;
    return this.http.put(this.url + endPoint, { amount });
  }

  transformCardNumber(num) {

    const modifyCard = num.toString().replace(/\D/g, '');

    let mini = modifyCard.substring(14, 16);
    let original = modifyCard.substring(14, 16);

    const first4 = modifyCard.substring(0, 4);
    const last4 = modifyCard.substring(12, 16);

    const full = 'xxxx-' + last4;
    mini = '**' + mini;
    original = first4 + ' **** **** ' + last4;

    const cardTypes = { full, mini, original };
    return { cardTypes };

  }


  getTypeEndpoint(type: 'CARD' | 'MORTGAGE' | 'DEPOSIT') {
    if (type === 'CARD') {
      return `${this.url}/data/cards`;
    } else if (type === 'DEPOSIT') {
      return `${this.url}/data/deposits`;
    } else if (type === 'MORTGAGE') {
      return `${this.url}/data/credits`;
    }
  }

  deleteAccount(account, type: 'CARD' | 'MORTGAGE' | 'DEPOSIT') {
    const url = `${this.getTypeEndpoint(type)}/${account.objectId}`;
    const { token } = JSON.parse(localStorage.getItem('User'));

    return this.http.delete(url, {
      headers: { 'user-token': token }
    });
  }

  updateAccount(account, type: 'CARD' | 'MORTGAGE' | 'DEPOSIT') {

    const url = `${this.getTypeEndpoint(type)}/${account.objectId}`;

    const { token } = JSON.parse(localStorage.getItem('User'));

    return this.http.put(url, account, {
      headers: { 'user-token': token }
    });
  }


  formatCardNumber(num: number) {
    const cardNumber = num.toString();
    const spaced = Array.from(cardNumber.toString()).reduce((a, b, i) => {
      if ((i + 1) % 4 === 0) {
        return a + b + ' ';
      }
      return a + b;
    });

    const hidden = spaced.split(' ').map((fourDigits, i) => {
      if (i > 0 && i < 3) {
        fourDigits = '****';
      }
      return fourDigits;
    }).join(' ');

    return { spaced, hidden };
  }


  addCard(card) {
    const url = `${this.url}/data/cards`;
    return this.http.post<ICard>(url, card).pipe(
      catchError(error => of(error.error))
    );
  }

  addCredit(card) {
    const url = `${this.url}/data/credits`;
    return this.http.post<ICredit>(url, card).pipe(
      catchError(error => of(error.error))
    );
  }

  addDeposit(card) {
    const url = `${this.url}/data/deposits`;
    return this.http.post<IDeposit>(url, card).pipe(
      catchError(error => of(error.error))
    );
  }
}
