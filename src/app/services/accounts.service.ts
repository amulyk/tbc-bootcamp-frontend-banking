import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { forkJoin, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { LoginService } from '../login.service';
import { ICard, ICredit, IDeposit } from './types/accounts';

@Injectable({
  providedIn: 'root'
})
export class AccountsService {
  public baseURL = 'https://api.backendless.com/B093B431-BA40-BFB4-FF22-46050E448800/A9B429C8-C84C-D894-FF06-E8783F872000';

  public cards: Array<object | ICard> = [];

  public credits: Array<object | ICredit> = [];

  public deposits: Array<object | IDeposit> = [];

  public allAccounts: Observable<object | ICard | IDeposit | ICredit>;

  formatCardNumber(num: number) {
    const cardNumber = num.toString();
    const spaced = Array.from(cardNumber.toString()).reduce((a, b, i) => {
      if ((i + 1) % 4 === 0) {
        return a + b + ' ';
      }
      return a + b;
    });

    const hidden = spaced.split(' ').map((fourDigits, i) => {
      if (i > 0 && i < 3) {
        fourDigits = '****';
      }
      return fourDigits;
    }).join(' ');

    return { spaced, hidden };
  }

  getDeposits() {
    const url = `${this.baseURL}/data/deposits`;

    const { token, userId } = JSON.parse(localStorage.getItem('User'));
    return this.http.get(url, {
      headers: { 'user-token': token }
    });
  }


  getCredits() {
    const url = `${this.baseURL}/data/credits`;

    const { token, userId } = JSON.parse(localStorage.getItem('User'));
    return this.http.get(url, {
      headers: { 'user-token': token }
    });
  }

  getCards() {
    const url = `${this.baseURL}/data/cards`;

    const { token, userId } = JSON.parse(localStorage.getItem('User'));
    return this.http.get(url, {
      headers: { 'user-token': token }
    });
  }

  getAllAccounts() {
    const cards = this.getCards().pipe(map((r: any) => {
      r.forEach((card) => {
        this.cards.push(card);
      });
      return r;
    }));
    const deposits = this.getDeposits().pipe(map((r: any) => {
      r.forEach((deposit) => {
        this.deposits.push(deposit);
      });
      return r;
    }));
    const credits = this.getCredits().pipe(map((r: any) => {
      r.forEach((credit) => {
        this.credits.push(credit);
      });
      return r;
    }));

    this.allAccounts = forkJoin([cards, deposits, credits])
      .pipe(map(response => {
        return response.reduce((accumulator, accounts) => accumulator.concat(accounts), []);
      }));
    return this.allAccounts;
  }

  getAccountType(account) {
    if (this.cards.includes(account)) {
      return 'CARD';
    } else if (this.deposits.includes(account)) {
      return 'DEPOSIT';
    } else if (this.credits.includes(account)) {
      return 'MORTGAGE';
    }
  }

  getTypeEndpoint(type: 'CARD' | 'MORTGAGE' | 'DEPOSIT') {
    if (type === 'CARD') {
      return `${this.baseURL}/data/cards`;
    } else if (type === 'DEPOSIT') {
      return `${this.baseURL}/data/deposits`;
    } else if (type === 'MORTGAGE') {
      return `${this.baseURL}/data/credits`;
    }
  }


  deleteAccount(account, type: 'CARD' | 'MORTGAGE' | 'DEPOSIT') {
    const url = `${this.getTypeEndpoint(type)}/${account.objectId}`;
    const { token } = JSON.parse(localStorage.getItem('User'));

    return this.http.delete(url, {
      headers: { 'user-token': token }
    });
  }

  updateAccount(account, type: 'CARD' | 'MORTGAGE' | 'DEPOSIT') {

    const url = `${this.getTypeEndpoint(type)}/${account.objectId}`;

    const { token } = JSON.parse(localStorage.getItem('User'));

    return this.http.put(url, account, {
      headers: { 'user-token': token }
    });
  }

  constructor(
    public http: HttpClient,
    public loginService: LoginService
  ) { }
}
