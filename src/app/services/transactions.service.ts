import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LoginService } from '../login.service';
import { CardService } from './card.service';

@Injectable({
  providedIn: 'root'
})
export class TransactionsService {
  private currencies = [{ value: 'USD' }, { value: 'GEL' }];
  private transferTypes = [{ value: 'Personal transfer' }, { value: 'Non personal transfer' }];
  private paymentSystems = [{ value: 'PayPal' }, { value: 'TBC' }];
  private baseUrl = 'http://api.backendless.com/B093B431-BA40-BFB4-FF22-46050E448800/A9B429C8-C84C-D894-FF06-E8783F872000';

  constructor(private http: HttpClient, private loginService: LoginService, private cardService: CardService) { }

  transfer(val) {
    const endPoint = '/data/transactions'
    console.log(val);
    const transaction = {
      recivierId:'',
      senderId: this.loginService.authenticatedUserId,
      image:'',
      title: '',
      type: '',
      cardId: '',
      amount:''
    }
    return this.http.post(this.baseUrl + endPoint, transaction)
  }

  getCurrencies() {
    return this.currencies;
  }

  getTransferTypes() {
    return this.transferTypes;
  }

  getPaymentSystems() {
    return this.paymentSystems;
  }
}
