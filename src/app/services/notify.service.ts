import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LoginService } from '../login.service';

@Injectable({
  providedIn: 'root'
})
export class Notify {

  url = 'https://api.backendless.com/B093B431-BA40-BFB4-FF22-46050E448800/A9B429C8-C84C-D894-FF06-E8783F872000';
  constructor(
    private http: HttpClient,
    private userService: LoginService
  ) { }

  notify(options, user = null) {
    const endPoint = '/data/notifications';
    const notifyUser = user ? user : this.userId;
    const body = { ...options, customerId: notifyUser };

    return this.http.post(this.url + endPoint, body);

  }

  get userId() {
    return this.userService.authenticatedUserId;
  }

}
