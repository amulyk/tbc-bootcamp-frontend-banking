import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { LoginService } from '../login.service';

@Injectable({
  providedIn: 'root'
})
export class BankTransactionsService {
  url = 'https://api.backendless.com/B093B431-BA40-BFB4-FF22-46050E448800/A9B429C8-C84C-D894-FF06-E8783F872000/data/';

  constructor(
    private http: HttpClient,
    private loginService: LoginService,
  ) { }

  getTransactions() {
    const transactionParams = new HttpParams().set('where', `receiverId='${this.userId}' or senderId='${this.userId}'`);
    return this.http.get(`${this.url}transactions`, { params: transactionParams });
  }

  getCards(type, cardId) {
    const cardParams = new HttpParams().set('where', `objectId='${cardId}'`);
    return this.http.get(`${this.url}${type}`, { params: cardParams });
  }

  get userId() {
    return this.loginService.authenticatedUserId;
  }

}
