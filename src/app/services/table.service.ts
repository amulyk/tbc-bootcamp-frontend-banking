import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TableService {

  constructor() { }

  ascSort(n, target) {
    let result = [];
    const keys = Object.keys(target[0]);
    const key = keys[n];

    if (typeof target[0][key] === 'number') {
      result = target.sort( (a, b) => {
        if (typeof a[key] === 'number') {
          return a[key] - b[key];
        }
      });
    } else {
      result = target.sort( (a, b) => {
        const x = a[key].toLowerCase();
        const y = b[key].toLowerCase();

        if (x < y) {
          return -1;
        } else {
          return 1;
        }
      });
    }
    return result;
  }

  descSort(n, target) {
    let result = [];
    const keys = Object.keys(target[0]);
    const key = keys[n];

    if (typeof target[0][key] === 'number') {
      result = target.sort( (a, b) => {
        if (typeof a[key] === 'number') {
          return b[key] - a[key];
        }
      });
    } else {
      result = target.sort( (a, b) => {
        const x = a[key].toLowerCase();
        const y = b[key].toLowerCase();

        if (x > y) {
          return -1;
        } else {
          return 1;
        }
      });
    }
    return result;
  }

}
