export const database = [
    {
        imgUrl: '../../assets/EUR.png',
        time: '00:00 Feb 26, 2019',
        cur: 'JPN',
        event: 'BoJ Core CPI (YoY)',
        importance: 'medium',
        actual: '0.5%',
        forecast: '+1.53%',
        previous: '0.20'
    },
    {
        time: '00:00 Feb 26, 2019',
        cur: 'EUR',
        event: 'GfK German Consumer Climate (Mar)',
        importance: 'medium',
        actual: '0.5%',
        forecast: '+1.53%',
        previous: '-0.1'
    },
    {
        time: '00:00 Feb 26, 2019',
        cur: 'USA',
        event: 'BoJ Core CPI (YoY)',
        importance: 'medium',
        actual: '0.5%',
        forecast: '+1.53%',
        previous: '-0.3'
    },
    {
        time: '00:00 Feb 26, 2019',
        cur: 'JPN',
        event: 'Industrial Production (MoM) (Jan)',
        importance: 'medium',
        actual: '0.5%',
        forecast: '+1.53%',
        previous: '0.77'
    },
    {
        time: '00:00 Mar 26, 2019',
        cur: 'USA',
        event: 'BoJ Core CPI (YoY)',
        importance: 'medium',
        actual: '0.5%',
        forecast: '+1.53%',
        previous: '0.21'
    },
    {
        time: '00:00 Feb 26, 2019',
        cur: 'EUR',
        event: 'GfK German Consumer Climate (Mar)',
        importance: 'medium',
        actual: '0.5%',
        forecast: '+1.53%',
        previous: '-0.53'
    },
    {
        time: '00:00 Feb 26, 2019',
        cur: 'GBP',
        event: 'Gross Mortgage Approvals',
        importance: 'medium',
        actual: '0.5%',
        forecast: '+1.53%',
        previous: '-0.53'
    },
    {
        time: '00:00 Feb 26, 2019',
        cur: 'EUR',
        event: 'GfK German Consumer Climate (Mar)',
        importance: 'medium',
        actual: '0.5%',
        forecast: '+1.52%',
        previous: '-0.53'
    },
];

