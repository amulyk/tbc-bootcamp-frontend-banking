import { Injectable } from '@angular/core';
import {database} from './dbCalendar';

@Injectable({
  providedIn: 'root'
})
export class TradingCalendarService {
  headers = ['Time', 'Cur', 'Event', 'Importance', 'Actual', 'Forecast', 'Previous'];

  constructor() { }

  getInfo() {
    return database;
  }
}
