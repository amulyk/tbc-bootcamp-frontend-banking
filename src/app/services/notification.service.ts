import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  private notifications = [];

  shift() {
    this.notifications.shift();
  }

  getNotifications() {
    return this.notifications;
  }

  close(notification) {
    this.notifications = this.notifications.filter(it => it !== notification);
  }

  push(notification) {
    this.notifications.push(notification);
  }
}
