import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { LoginService } from '../login.service';

interface IUser {
  firstName: string;
  lastName: string;
  email: string;
  phone: string;
  language: string;
  sex: string;
  about: string;
}

@Injectable({
  providedIn: 'root'
})
export class SettingsGeneralInformationService {
  host = 'https://api.backendless.com/B093B431-BA40-BFB4-FF22-46050E448800/A9B429C8-C84C-D894-FF06-E8783F872000';
  constructor(
    private httpClient: HttpClient,
    private loginService: LoginService) { }

  addUser(employee: IUser): Observable<IUser> {
    const id = this.loginService.authenticatedUserId;

    const url = `${this.host}/data/customers/${id}`;

    return this.httpClient.put<IUser>(url, employee);
  }

  getUser() {
    const id = this.loginService.authenticatedUserId;
    const url = `${this.host}/data/customers/${id}`;

    return this.httpClient.get(url).pipe(map((user: IUser) => {

      const {
        firstName,
        lastName,
        email,
        phone,
        language,
        sex,
        about
      } = user;

      return {
        name: firstName,
        surname: lastName,
        email,
        phone,
        language,
        sex,
        about
      };
    }));
  }
}
