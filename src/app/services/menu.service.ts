import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MenuService {

  private menus = [
    {
      link: '/dashboard',
      icon: 'dashboard',
      text: 'DASHBOARD',
    },
    {
      link: '/account',
      icon: 'account',
      text: 'ACCOUNT',
    },
    {
      link: '/setting',
      icon: 'setting',
      text: 'SETTINGS',
    },
    {
      link: '/accounts',
      icon: 'accounts',
      text: 'ACCOUNTS',
    },
    {
      link: '/transactions',
      icon: 'transactions',
      text: 'TRANSACTIONS',
    },
    {
      link: '/payments',
      icon: 'payments',
      text: 'PAYMENTS',
    },
    {
      link: '/stocks',
      icon: 'stocks',
      text: 'STOCKS',
    },
    {
      link: '/news',
      icon: 'news',
      text: 'NEWS',
    },
    {
      link: '/error',
      icon: 'setting',
      text: 'UNKNOWN',
    },
  ];
  constructor() { }

  getMenus() {
    return this.menus;
  }

}
