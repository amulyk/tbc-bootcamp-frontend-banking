import { Component, OnInit } from '@angular/core';
import { TradingCurrenciesService } from '../services/trading-currencies.service';

@Component({
  selector: 'app-trading-currencies-corporate',
  templateUrl: './trading-currencies-corporate.component.html',
  styleUrls: ['./trading-currencies-corporate.component.scss']
})

export class TradingCurrenciesCorporateComponent implements OnInit {
  currentPage;

  items = this.getCurrencies();


  constructor(
    private tradingCurrenciesService: TradingCurrenciesService) { }

  ngOnInit() {
    if (typeof this.currentPage === 'undefined') {
      this.currentPage = this.items;
    }
  }

  get headers() {
    return this.tradingCurrenciesService.headers;
  }

  getCurrencies() {
    return this.tradingCurrenciesService.getCurrencies();
  }

  newPage(event) {
    this.currentPage = event;
  }

}
