import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent implements OnInit {
  pageCount;
  pages = [];
  index = 1;
  contentPage;

  @Input() perPage;
  @Input() content;
  @Output() emitter = new EventEmitter();


  constructor() { }

  ngOnInit() {
      this.pageCount = Math.ceil(this.content.length / this.perPage);
      this.contentPage = this.content.slice(0, this.perPage);
      for (let i = 1; i <= 5; i++) {
        this.pages.push(i);
      }
      this.emitter.emit(this.contentPage);
    }

  nextPage(index) {
    const nextIndex = (index - 1) * this.perPage;
    this.contentPage = this.content.slice(nextIndex, nextIndex + this.perPage);
    this.index = index;

    let startIndex;
    let endIndex;

    if (index < 4) {
      startIndex = 1;
      endIndex = 5;
    } else {
      startIndex = index - 2;
      endIndex = index + 2;
    }

    if (index === this.pageCount - 1) {
      endIndex = this.pageCount;
    }

    if (index === this.pageCount) {
      endIndex = index;
    }

    this.pages = [];
    for (let i = startIndex; i <= endIndex; i++) {
      this.pages.push(i);
    }

    this.emitter.emit(this.contentPage);
  }
}
