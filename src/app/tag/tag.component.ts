import { Component, OnInit, Output, Input } from '@angular/core';
import { EventEmitter } from '@angular/core';

@Component({
  selector: 'app-tag',
  templateUrl: './tag.component.html',
  styleUrls: ['./tag.component.scss']
})
export class TagComponent implements OnInit {

  isTag = true;

  @Input() title;
  @Input() type;
  @Output() tagClose = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  closeTag() {
    this.isTag = false;
    this.tagClose.emit();
  }

}
