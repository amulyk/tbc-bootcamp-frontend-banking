import {Component, forwardRef, Input, ViewEncapsulation} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';

@Component({
  selector: 'app-textarea-with-emoji',
  templateUrl: './textarea-with-emoji.component.html',
  styleUrls: ['./textarea-with-emoji.component.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => TextareaWithEmojiComponent),
    multi: true
  }]
})
export class TextareaWithEmojiComponent implements  ControlValueAccessor {
  @Input() required: boolean;
  @Input() disabled: boolean;

  value: string;

  private onChange = Function;
  private onTouched = Function;

  constructor() {
    this.value = '';
  }

  emojiAdded(v: any) {
    this.changeValue(v.toString());
  }

  changeValue(value: any) {
    this.writeValue(value);
    this.onChange(this.value);
    this.onTouched();
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean): void {
  }

  writeValue(value: string): void {
    this.value = value ? value : '';
  }
}
