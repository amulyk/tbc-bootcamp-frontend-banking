import { AbstractControl, ValidationErrors } from '@angular/forms';

export class CardValidators {
    static Required(control: AbstractControl): ValidationErrors | null {
        if ((control.value as string) === '') {
            return { Required: { message: 'This Field is required' } };
        }
        return null;
    }

    static InvalidName(control: AbstractControl): ValidationErrors | null {
        const pattern = /^[A-Za-z ]+$/i;
        if (((control.value as string) !== '') && (!pattern.test(control.value))) {
            return { InvalidName: { message: 'Name must contain only english charachters' } };
        }
        return null;
    }

    static InvalidAccountNumber(control: AbstractControl): ValidationErrors | null {
        const pattern = /^[A-Za-z0-9\+]+$/i;
        if (((control.value as string) !== '') && (!pattern.test(control.value))) {
            return { InvalidAccountNumber: { message: 'Provide a valid account number' } };
        }
        return null;
    }

    static InvalidCardNumber(control: AbstractControl): ValidationErrors | null {
        const pattern = /^[0-9\+]+$/i;
        if (((control.value as string) !== '') && (control.value as string).length !== 16) {
            return { InvalidCardNumber: { message: 'Provide a valid card number example 1234 1234 1234 1234' } };
        } else if (((control.value as string) !== '') && (!pattern.test(control.value))) {
            return { InvalidCardNumber: { message: 'Provide a valid card number' } };
        }
        return null;
    }


    static InvalidRate(control: AbstractControl) {
        const pattern = /^[0-9\.]+$/i;

        if (control.value !== '' && !pattern.test(control.value)) {
            return { InvalidRate: { message: 'Please provide a valid rate' } };
        }
        return null;
    }

    static InvalidAmount(control: AbstractControl) {
        const pattern = /^[0-9\.]+$/i;

        if (control.value !== '' && !pattern.test(control.value)) {
            return { InvalidAmount: { message: 'Please provide a valid amount' } };
        }
        return null;
    }


    static InvalidInputsForCardForm(control: AbstractControl) {
        const cardName = control.get('CARDcardName').valid;
        const accountNumber = control.get('CARDaccountNumber').valid;
        const cardholder = control.get('CARDcardholder').valid;
        const expirationDate = control.get('CARDexpirationDate').valid;
        const availableAmount = control.get('CARDavailableAmount').valid;
        if (cardName && accountNumber && cardholder && expirationDate && availableAmount) {
            return null;
        }
        return { InvalidInputsForCardForm: true };
    }

    static InvalidInputsForCredit(control: AbstractControl) {
        const name = control.get('CREname').valid;
        const accountNumber = control.get('CREaccountNumber').valid;
        const rate = control.get('CRErate').valid;
        const startAndExpirationDate = control.get('CREstartAndExpirationDate').valid;
        const startingAmount = control.get('CREstartingAmount').valid;
        const paidAmount = control.get('CREpaidAmount').valid;
        if (name && accountNumber && rate && startAndExpirationDate && startingAmount && paidAmount) {
            return null;
        }
        return { InvalidInputsForCredit: true };
    }

    static InvalidInputsForDeposit(control: AbstractControl) {
        const name = control.get('DEPname').valid;
        const accountNumber = control.get('DEPaccountNumber').valid;
        const rate = control.get('DEPrate').valid;
        const startAndExpirationDate = control.get('DEPstartAndExpirationDate').valid;
        const amount = control.get('DEPamount').valid;
        const accuredInterest = control.get('DEPaccuredInterest').valid;
        if (name && accountNumber && rate && startAndExpirationDate && amount && accuredInterest) {
            return null;
        }
        return { InvalidInputsForDeposit: true };
    }

}
