import { LoginService } from './../login.service';
import { ActivatedRoute, Router } from '@angular/router';
import { CardService } from './../services/card.service';
import { CardValidators } from './CardValidators';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { Component, OnInit, ViewEncapsulation, EventEmitter, Input } from '@angular/core';
import * as moment from 'moment';
import * as localization from 'moment/locale/fr';
import { LocaleConfig } from 'ngx-daterangepicker-material';
moment.locale('en', localization);

@Component({
  selector: 'app-create-card',
  templateUrl: './create-card.component.html',
  styleUrls: ['./create-card.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CreateCardComponent implements OnInit {
  type ;
  alwaysShowCalendars;
  isCardTaken = false;

  date = new EventEmitter();
  selected: any;
  locale: LocaleConfig = {
    applyLabel: 'Appliquer',
    customRangeLabel: ' - ',
    daysOfWeek: moment.weekdaysMin(),
    monthNames: moment.monthsShort(),
    firstDay: moment.localeData().firstDayOfWeek(),
  };


  cardForm: FormGroup;
  creditForm: FormGroup;
  depositForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder, private cardService: CardService,
    private route: ActivatedRoute, private loginService: LoginService, private router: Router
    ) {
    this.alwaysShowCalendars = true;

    this.cardForm = this.formBuilder.group({
      CARDcardName: ['', [CardValidators.Required]],
      CARDcardNumber: ['', [CardValidators.Required, CardValidators.InvalidCardNumber]],
      CARDaccountNumber: ['', [CardValidators.Required, CardValidators.InvalidAccountNumber]],
      CARDcardholder: ['', [CardValidators.Required, CardValidators.InvalidName]],
      CARDexpirationDate: ['', CardValidators.Required],
      CARDavailableAmount: ['', [CardValidators.Required, CardValidators.InvalidAmount]],
      CARDsecurity: ''
    }, {
        validators: CardValidators.InvalidInputsForCardForm
      });

    this.creditForm = this.formBuilder.group({
      CREname: ['', [CardValidators.Required]],
      CREcardNumber: ['', [CardValidators.Required, CardValidators.InvalidCardNumber]],
      CREaccountNumber: ['', [CardValidators.Required, CardValidators.InvalidAccountNumber]],
      CRErate: ['', [CardValidators.Required, CardValidators.InvalidRate]],
      CREstartAndExpirationDate: ['', CardValidators.Required],
      CREstartingAmount: ['', [CardValidators.Required, CardValidators.InvalidAmount]],
      CREpaidAmount: ['', [CardValidators.Required, CardValidators.InvalidAmount]]
    }, {
        validators: CardValidators.InvalidInputsForCredit
      });

    this.depositForm = this.formBuilder.group({
      DEPname: ['', [CardValidators.Required]],
      DEPcardNumber: ['', [CardValidators.Required, CardValidators.InvalidCardNumber]],
      DEPaccountNumber: ['', [CardValidators.Required, CardValidators.InvalidAccountNumber]],
      DEPrate: ['', [CardValidators.Required, CardValidators.InvalidRate]],
      DEPstartAndExpirationDate: ['', CardValidators.Required],
      DEPamount: ['', [CardValidators.Required, CardValidators.InvalidAmount]],
      DEPaccuredInterest: ['', [CardValidators.Required, CardValidators.InvalidRate]]
    }, {
        validators: CardValidators.InvalidInputsForDeposit
      });

  }

  ngOnInit() {
    this.type = this.route.snapshot.queryParamMap.get('type');
    this.CARDcardNumber.valueChanges.subscribe(() => {
      if (this.isCardTaken) {
        this.isCardTaken = false;
      }
    });
    this.CREcardNumber.valueChanges.subscribe(() => {
      if (this.isCardTaken) {
        this.isCardTaken = false;
      }
    });
    this.DEPcardNumber.valueChanges.subscribe(() => {
      if (this.isCardTaken) {
        this.isCardTaken = false;
      }
    });
  }


  // cardFrom getters:

  get CARDcardName() {
    return this.cardForm.get('CARDcardName') as FormControl;
  }
  get CARDaccountNumber() {
    return this.cardForm.get('CARDaccountNumber') as FormControl;
  }
  get CARDcardNumber() {
    return this.cardForm.get('CARDcardNumber') as FormControl;
  }
  get CARDcardholder() {
    return this.cardForm.get('CARDcardholder') as FormControl;
  }
  get CARDexpirationDate() {
    return this.cardForm.get('CARDexpirationDate') as FormControl;
  }
  get CARDavailableAmount() {
    return this.cardForm.get('CARDavailableAmount') as FormControl;
  }
  get CARDsecurity() {
    return this.cardForm.get('CARDsecurity') as FormControl;
  }

  // creditForm getters:

  get CREcardNumber() {
    return this.creditForm.get('CREcardNumber') as FormControl;
  }
  get CREname() {
    return this.creditForm.get('CREname') as FormControl;
  }
  get CREaccountNumber() {
    return this.creditForm.get('CREaccountNumber') as FormControl;
  }
  get CRErate() {
    return this.creditForm.get('CRErate') as FormControl;
  }
  get CREstartAndExpirationDate() {
    return this.creditForm.get('CREstartAndExpirationDate') as FormControl;
  }
  get CREstartingAmount() {
    return this.creditForm.get('CREstartingAmount') as FormControl;
  }
  get CREpaidAmount() {
    return this.creditForm.get('CREpaidAmount') as FormControl;
  }

  // depositForm getters:

  get DEPcardNumber() {
    return this.depositForm.get('DEPcardNumber') as FormControl;
  }
  get DEPname() {
    return this.depositForm.get('DEPname') as FormControl;
  }
  get DEPaccountNumber() {
    return this.depositForm.get('DEPaccountNumber') as FormControl;
  }
  get DEPrate() {
    return this.depositForm.get('DEPrate') as FormControl;
  }
  get DEPstartAndExpirationDate() {
    return this.depositForm.get('DEPstartAndExpirationDate') as FormControl;
  }
  get DEPamount() {
    return this.depositForm.get('DEPamount') as FormControl;
  }
  get DEPaccuredInterest() {
    return this.depositForm.get('DEPaccuredInterest') as FormControl;
  }

  // datepicker methods:

  isInvalidDate(date) {
    return date.weekday() === 0;
  }
  isCustomDate(date) {
    return (
      date.weekday() === 0 ||
      date.weekday() === 6
    ) ? 'mycustomdate' : false;
  }

  //main methods

  addCard() {
    const card = {
      amount: parseInt(this.cardForm.get('CARDavailableAmount').value, 10),
      name: this.cardForm.get('CARDcardName').value,
      cardNumber: parseInt(this.cardForm.get('CARDcardNumber').value, 10),
      customerId: this.loginService.authenticatedUserId,
      expirationDate: this.cardForm.get('CARDexpirationDate').value.startDate.toISOString(),
      additionalSecurity: this.cardForm.get('CARDsecurity').value ? this.cardForm.get('CARDsecurity').value : false,
      cardholder: this.cardForm.get('CARDcardholder').value,
      accountNumber: this.cardForm.get('CARDaccountNumber').value,
    };
    this.cardService.addCard(card).subscribe(value => {
      if (value.code === 1155) {// dublicate value error
        if (value.errorData.columnName === 'cardNumber') {
          this.isCardTaken = true;
          return;
        }
      }
      const event = new CustomEvent('addCard', { 'detail': 'success' });
      document.dispatchEvent(event);
      this.router.navigate(['account']);
    });
  }

  addDeposit() {
    const card = {
      startDate: this.depositForm.get('DEPstartAndExpirationDate').value.startDate.toISOString(),
      expirationDate: this.depositForm.get('DEPstartAndExpirationDate').value.endDate.toISOString(),
      rate: parseInt(this.depositForm.get('DEPrate').value, 10),
      cardNumber: parseInt(this.depositForm.get('DEPcardNumber').value, 10),
      accountNumber: this.depositForm.get('DEPaccountNumber').value,
      customerId:  this.loginService.authenticatedUserId,
      accuredInterest: parseInt(this.depositForm.get('DEPaccuredInterest').value, 10),
      amount: parseInt(this.depositForm.get('DEPamount').value, 10),
      name: this.depositForm.get('DEPname').value,
    };
    this.cardService.addDeposit(card).subscribe(value => {
      if (value.code === 1155) {// dublicate value error
        if (value.errorData.columnName === 'cardNumber') {
          this.isCardTaken = true;
          return;
        }
      }
      const event = new CustomEvent('addCard', { 'detail': 'success' });
      document.dispatchEvent(event);
      this.router.navigate(['account']);
    });
  }


  addCredit() {
    const card = {
      paid: parseInt(this.creditForm.get('CREpaidAmount').value, 10),
      amount: parseInt(this.creditForm.get('CREstartingAmount').value, 10),
      name: this.creditForm.get('CREname').value,
      cardNumber: parseInt(this.creditForm.get('CREcardNumber').value, 10),
      rate: parseInt(this.creditForm.get('CRErate').value, 10),
      customerId:  this.loginService.authenticatedUserId,
      startDate: this.creditForm.get('CREstartAndExpirationDate').value.startDate.toISOString(),
      accountNumber: this.creditForm.get('CREaccountNumber').value,
      expirationDate: this.creditForm.get('CREstartAndExpirationDate').value.endDate.toISOString(),
    };
    this.cardService.addCredit(card).subscribe(value => {
      if (value.code === 1155) {// dublicate value error
        if (value.errorData.columnName === 'cardNumber') {
          this.isCardTaken = true;
          return;
        }
      }
      const event = new CustomEvent('addCard', { 'detail': 'success' });
      document.dispatchEvent(event);
      this.router.navigate(['account']);
    });
  }

}
