interface IResponse {
  objectId: string;
  'user-token': string;
}

interface ICustomer {
  accountNumber?: string;
  addressLine?: string;
  bankTransactionsLimit?: number;
  cashWithdrawalsLimit?: number;
  city?: string;
  email?: string;
  firstName?: string;
  isCommentsEnabled?: boolean;
  isNotificationsEnabled?: boolean;
  isOfferUpdatesEnabled?: boolean;
  isProductUpdatesEnabled?: boolean;
  language?: string;
  lastName?: string;
  onlinePaymentsLimit?: number;
  phone?: string;
  plan?: string;
  primaryCard?: string;
  securityAnswer1?: string;
  securityAnswer2?: string;
  sequrityQuestion1?: string;
  securityQuestion2?: string;
  sex?: string;
  state?: string;
  userId: string;
  zip?: string;
  objectId?: string;
  created?: string;
  updated?: string;
}

export {ICustomer, IResponse};
