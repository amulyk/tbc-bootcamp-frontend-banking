import { Component, forwardRef, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { ControlValueAccessor, FormControl, FormGroup, NG_VALUE_ACCESSOR } from '@angular/forms';
import { animate, state, style, transition, trigger } from '@angular/animations';

@Component({
  selector: 'app-select-fields',
  templateUrl: './select-fields.component.html',
  styleUrls: ['./select-fields.component.scss'],
  animations: [
    trigger('openClose', [
      state('open', style({
        opacity: 1,
        display: 'block'
      })),
      state('close', style({
        opacity: 0,
        display: 'none'
      })),
      transition('open => close', [
        animate('0.5s')
      ]),
      transition('close => open', [
        animate('0.1s')
      ])
    ])
  ],
  encapsulation: ViewEncapsulation.None,
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => SelectFieldsComponent),
    multi: true
  }]
})
export class SelectFieldsComponent implements OnInit, ControlValueAccessor {
  @Input() list = [];
  @Input() placeholder = '';
  showDropDown = false;
  formGroup = new FormGroup({dropdownInput: new FormControl()});
  selectedItem;

  onModelChange = Function;
  onTouch = Function;

  constructor() {
  }

  ngOnInit() {
    this.placeholder = this.getDefaultPlaceholder();
  }

  getDefaultPlaceholder() {
    if (this.list && this.list.length > 0) {
      return this.list[0].value;
    }
    return 'Placeholder';
  }

  selectedValue(item, index) {
    this.formGroup.get('dropdownInput').setValue(item.value);
    this.selectedItem = index;
    this.onModelChange(item.value);
    this.onTouch();
  }

  showDropdown() {
    this.showDropDown = !this.showDropDown;
  }

  hideDropdown() {
    this.showDropDown = false;
  }

  registerOnChange(fn: any): void {
    this.onModelChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouch = fn;
  }

  setDisabledState(isDisabled: boolean): void {
  }

  writeValue(obj: any): void {
    this.formGroup.get('dropdownInput').setValue(obj);
    this.selectedItem = this.list.findIndex(item => {
      return item.value === obj;
    });
  }

}
