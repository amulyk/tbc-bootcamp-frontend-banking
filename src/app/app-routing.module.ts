import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WrapperComponent } from './wrapper/wrapper.component';
import { AccountComponent } from './account/account.component';
import { LoginComponent } from './login/login.component';
import { UserGuard } from './user.guard';
import { PaymentLimitsComponent } from './payment-limits/payment-limits.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { BankTransactionsComponent } from './bank-transactions/bank-transactions.component';
import { NewsArticleComponent } from './news-article/news-article.component';
import { RegistrationComponent } from './registration/registration.component';
import {AccountsCorporateComponent} from './accounts-corporate/accounts-corporate.component';
import {SettingNoteComponent} from './setting-note/setting-note.component';
import { ErrorComponent } from './error/error.component';
import { SettingsComponent } from './settings/settings.component';
import { SettingsSecurityComponent } from './settings-security/settings-security.component';
import { CreateCardComponent } from './create-card/create-card.component';
import { SettingsGeneralInformationsComponent } from './settings-general-informations/settings-general-informations.component';
import { NewsComponent } from './news/news.component';
import { PaymentsComponent } from './payments/payments.component';
import { CardsComponent } from './cards/cards.component';
import { DashboardEmptyAccountComponent } from './dashboard-empty-account/dashboard-empty-account.component';
import {NewsSliderComponent} from './news-slider/news-slider.component';
import { StocksComponent } from './stocks/stocks.component';

const routes: Routes = [
  { path: 'login', data: {header: 'Login'}, component: LoginComponent },
  { path: 'register', data: {header: 'Register'}, component: RegistrationComponent },
  { path: '404error', data: {header: 'error'}, component: ErrorComponent},
  {
    path: '',
    component: WrapperComponent,
    canActivate: [UserGuard],
    children: [
      { path: 'setting', data: { header: 'Settings' }, component: SettingsComponent,
      children: [
        { path: 'generalinformation', component: SettingsGeneralInformationsComponent},
        { path: 'security', component: SettingsSecurityComponent },
        { path: 'paymentlimits', component: PaymentLimitsComponent},
        { path: 'notifications', component: SettingNoteComponent},
        { path: '', redirectTo: 'generalinformation', pathMatch: 'full'}
      ]},
      { path: 'dashboard', data: { header: 'Dashboard' }, component: DashboardComponent },
      { path: 'account', data: { header: 'Account' }, component: AccountComponent },
      { path: 'account/:id', data: { header: 'Account' }, component: CardsComponent },
      { path: 'cards/new', data: { header: 'createcard' }, component: CreateCardComponent },
      { path: 'setting', data: { header: 'Settings' }, component: SettingsComponent },
      { path: 'transactions', data: { header: 'Transactions' }, component: BankTransactionsComponent },
      { path: 'products', data: { header: 'Prodcuts' }, component: WrapperComponent },
      { path: 'payments', data: { header: 'Payments' }, component: PaymentsComponent },
      { path: 'stocks', data: { header: 'Stocks' }, component: StocksComponent },
      { path: 'cards/:id', data: { header: 'Card' }, component: WrapperComponent },
      { path: 'accounts', data: { header: 'Accounts' }, component: AccountsCorporateComponent },
      { path: 'add-deposit', data: { header: 'Deposit' }, component: WrapperComponent },
      { path: 'add-creditcard', data: { header: 'Credit card' }, component: WrapperComponent },
      { path: 'news', data: { header: 'News' }, component: NewsComponent },
      { path: 'news-slider', data: { header: 'News' }, component: NewsSliderComponent },
      { path: 'news/:id', data: { header: 'News' }, component: NewsArticleComponent },
      { path: 'emptyaccount', data: { header: 'Accounts' }, component: DashboardEmptyAccountComponent },
    ]
  },
  { path: 'error', redirectTo: '404error' },
  { path: '**', redirectTo: '404error' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
