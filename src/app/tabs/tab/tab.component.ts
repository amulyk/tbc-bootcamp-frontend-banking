import { Component, Input } from '@angular/core';
import { animate, style, state, trigger, transition, keyframes } from '@angular/animations';

@Component({
  selector: 'app-tab',
  templateUrl: './tab.component.html',
  styleUrls: ['./tab.component.scss'],
  animations: [trigger('activeOrNot', [
    state('inactive', style({opacity: 0, left: '-100%'})),
    state('active', style({opacity: 1, 'z-index': 2})),
    transition('inactive => active', [
      animate('1s')
    ]), transition('active => inactive', [
      animate('1s', keyframes([
        style({right: '-100%', opacity: 0, offset: 1})
      ]))
    ])
  ])]
})
export class TabComponent {

  @Input() title: string;
  @Input() active = false;

}

