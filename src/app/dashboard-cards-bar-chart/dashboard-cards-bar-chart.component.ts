import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-dashboard-cards-bar-chart',
  templateUrl: './dashboard-cards-bar-chart.component.html',
  styleUrls: ['./dashboard-cards-bar-chart.component.scss']
})
export class DashboardCardsBarChartComponent implements OnInit {

  change = 0;

  @Input() data;

  constructor() { }

  ngOnInit() {
    this.change = this.data.cardIncomes.length >= 2 ?
      Math.round(this.data.cardIncomes[this.data.cardIncomes.length - 1].amount / this.data.cardIncomes[this.data.cardIncomes.length - 2].amount) / 100 : 0;
  }

  avg() {
    const total = this.data.cardIncomes.reduce((sum, item) => sum + item.amount, 0);

    return Math.floor((total / this.data.cardIncomes.length)) || 0;
  }

  roundUp(amount) {
    return `${Math.floor(amount / 1000)}k`;
  }

}
