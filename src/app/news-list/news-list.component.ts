import { Component, OnInit } from '@angular/core';
import { NewsListService } from '../news-list.service';
import { trigger, state, style } from '@angular/animations';
import { Router } from '@angular/router';

@Component({
  selector: 'app-news-list',
  templateUrl: './news-list.component.html',
  styleUrls: ['./news-list.component.scss'],
  animations: [
    trigger('loadUnload', [
      state('load', style({
        display: 'none'
      })),
      state('unload', style({
        display: 'block'
      }))
    ])
  ]
})
export class NewsListComponent implements OnInit {
  counter = 5;
  numStart = 5;
  datas = [];
  showMore = true;
  showLess = false;
  loader = true;

  constructor(private newsListService: NewsListService) { }

  ngOnInit() {
    this.datas.push(this.newsListService.getData(this.numStart)
    .subscribe(value => {
      this.loader = !this.loader;
      this.datas = value;
    }));
  }

  addInfo() {

    if (this.showMore) {
      this.loader = !this.loader;

      this.datas.push(this.newsListService.getData(this.numStart += this.counter)
      .subscribe(value => {
        this.loader = !this.loader;
        this.datas = value;
      }));

      if (!this.showLess) {
        this.showLess = !this.showLess;
      }
    }
  }

  showLessinfo() {
    if (this.showLess) {
      this.loader = !this.loader;

      this.datas.push(this.newsListService.getData(this.numStart -= this.counter)
      .subscribe(value => {
        this.datas = value;
        this.loader = !this.loader;
      }));

      if (!this.showMore) {
        this.showMore = !this.showMore;
      }

      if (this.numStart <= this.counter) {
        this.showLess = !this.showLess;
      }
    }
  }
}
