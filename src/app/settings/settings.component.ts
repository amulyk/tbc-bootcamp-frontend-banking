import { Component, OnInit } from '@angular/core';
import { CardService } from '../services/card.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {
  deposits = [];
  credits = [];

  constructor(private cardService: CardService) {
    this.cardService.getCredits().subscribe( value => {
      this.credits = value;
    });

    this.cardService.getDeposits().subscribe( value => {
      this.deposits = value;
    });
   }

  ngOnInit() {
  }

}
