import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { PaymentlimitsService } from '../paymentlimits.service';

@Component({
  selector: 'app-payment-limits',
  templateUrl: './payment-limits.component.html',
  styleUrls: ['./payment-limits.component.scss']
})

export class PaymentLimitsComponent implements OnInit {
  frm: FormGroup;
  inputData = {};
  paymentLimitsObj = this.paymentLimitsService.paymentLimitsObj;
  step = 1;
  progressBarValueObject: [] | any = [];
  val1;
  val2;
  color = ['primary', 'secondary-3'];
  showDebit = false;
  showCredit = false;
  CardAmounts = {
    card: 0,
    credit: 0
  };

  constructor(
    private check: FormBuilder,
    private paymentLimitsService: PaymentlimitsService,
  ) { }

  ngOnInit() {
    this.paymentLimitsService.getCardValues().subscribe(
      data => {
        if (data && Object.keys(data).length > 0) {
          this.showDebit = true;
          const objLength = Object.keys(data).length;
          for (let i = 0; i < objLength; i++) {
            this.CardAmounts.card += parseFloat(data[i].amount);
            this.val1 = this.CardAmounts.card.toFixed(2);
            this.insertInputValues();
          }
        }
      }
    );

    this.paymentLimitsService.getCreditCardValues().subscribe(
      data => {
        if (data && Object.keys(data).length > 0) {
          this.showCredit = true;
          const objLength = Object.keys(data).length;
          for (let i = 0; i < objLength; i++) {
            this.CardAmounts.credit += parseFloat(data[i].amount);
            this.val2 = this.CardAmounts.credit.toFixed(2);
            this.insertInputValues();
          }
        }
      }
    );
  }

  insertInputValues() {
    this.paymentLimitsService.getLimitValues().subscribe((data) => {
      this.frm = this.check.group({
        cashWithdrawalsLimit: [data.cashWithdrawalsLimit, [Validators.required]],
        bankTransactionsLimit: [data.bankTransactionsLimit, [Validators.required]]
      });
      this.getProgressBarValues();
    });
  }

  onSubmit() {
    this.inputData = this.frm.value;
    const keys = Object.keys(this.inputData);
    for (const key of keys) {
      this.inputData[key] = parseFloat(this.inputData[key]);
    }
    this.update();
  }


  onPlus(obj: string) {
    switch (obj) {
      case 'cashWithdrawalsLimit':
        this.frm.get('cashWithdrawalsLimit').setValue(parseFloat(this.frm.value.cashWithdrawalsLimit) + this.step);
        this.progressBarValidator('cashWithdrawalsLimit', 'cashWithdrawalsLimit', this.val1);
        break;
      case 'bankTransactionsLimit':
        this.frm.get('bankTransactionsLimit').setValue(parseFloat(this.frm.value.bankTransactionsLimit) + this.step);
        this.progressBarValidator('bankTransactionsLimit', 'bankTransactionsLimit', this.val2);
        break;
      default:
        console.log('Oops, something wrong here...');
    }
  }

  onMinus(obj: string) {
    switch (obj) {
      case 'cashWithdrawalsLimit':
        this.frm.get('cashWithdrawalsLimit').setValue(parseFloat(this.frm.value.cashWithdrawalsLimit) - this.step);
        this.progressBarValidator('cashWithdrawalsLimit', 'cashWithdrawalsLimit', this.val1);
        break;
      case 'bankTransactionsLimit':
        this.frm.get('bankTransactionsLimit').setValue(parseFloat(this.frm.value.bankTransactionsLimit) - this.step);
        this.progressBarValidator('bankTransactionsLimit', 'bankTransactionsLimit', this.val2);
        break;
      default:
        console.log('Oops, something wrong here...');
    }
  }

  getProgressBarValues() {
    this.progressBarValidator('cashWithdrawalsLimit', 'cashWithdrawalsLimit', this.val1);
    this.progressBarValidator('bankTransactionsLimit', 'bankTransactionsLimit', this.val2);
  }

  progressBarValidator(type: string, element: string, val: number) {
    if (type) {
      this.progressBarValueObject[type] = (Math.round(val) / this.frm.get(element).value) * 100;
      if (this.progressBarValueObject[type] > 100) {
        this.progressBarValueObject[type] = 100;
      }
    }
  }

  changeVal(str: string) {
    switch (str) {
      case 'cashWithdrawalsLimit':
        this.progressBarValueObject[str] = this.val1 / this.frm.get(str).value * 100;
        if (this.progressBarValueObject[str] > 100) {
          this.progressBarValueObject[str] = 100;
        }
        break;
      case 'bankTransactionsLimit':
        this.progressBarValueObject[str] = this.val2 / this.frm.get(str).value * 100;
        if (this.progressBarValueObject[str] > 100) {
          this.progressBarValueObject[str] = 100;
        }
        break;
      default:
        console.log('Oops, this type we have not !');
    }
  }

  update() {
    this.paymentLimitsService.updateForm(this.inputData).subscribe();
  }
  get debitsProgress() {
    return this.progressBarValueObject.cashWithdrawalsLimit;
  }

  get creditsProgress() {
    return this.progressBarValueObject.bankTransactionsLimit;
  }

}
