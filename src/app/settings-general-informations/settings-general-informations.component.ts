import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { SettingsGeneralInformationService } from '../services/settings-general-information.service';
import { LoginService } from '../login.service';
import {NotificationService} from '../services/notification.service';

@Component({
  selector: 'app-settings-general-informations',
  templateUrl: './settings-general-informations.component.html',
  styleUrls: ['./settings-general-informations.component.scss']
})
export class SettingsGeneralInformationsComponent implements OnInit {
  generalForm;
  customer;
  updateUser = false;
  requiredFields = false;
  languageList = [{ value: 'english' }, { value: 'georgian' }];
  list = [{ value: 'male' }, { value: 'female' }];

  constructor(
    private formBuilder: FormBuilder,
    private userService: SettingsGeneralInformationService,
    private notificationService: NotificationService
  ) { }

  ngOnInit() {
    this.generalForm = this.formBuilder.group({
      name: ['', Validators.required],
      surname: ['', Validators.required],
      email: ['', [Validators.email, Validators.required]],
      phone: ['', [Validators.required, Validators.pattern(/^[0-9]+$/), Validators.minLength(7), Validators.maxLength(7)]],
      language: [''],
      sex: [''],
      about: [''],
    });

    const employee = this.userService.getUser().subscribe((data) => {
      this.customer = data;
      this.generalForm.get('name').setValue(this.customer.name);
      this.generalForm.get('surname').setValue(this.customer.surname);
      this.generalForm.get('email').setValue(this.customer.email);
      this.generalForm.get('phone').setValue(this.customer.phone);
      this.generalForm.get('language').setValue(this.customer.language);
      this.generalForm.get('sex').setValue(this.customer.sex);
      this.generalForm.get('about').setValue(this.customer.about);
    });
  }

  get name() {
    return this.generalForm.get('name') as FormControl;
  }
  get surname() {
    return this.generalForm.get('surname') as FormControl;
  }
  get email() {
    return this.generalForm.get('email') as FormControl;
  }
  get phone() {
    return this.generalForm.get('phone') as FormControl;
  }
  get language() {
    return this.generalForm.get('language') as FormControl;
  }
  get sex() {
    return this.generalForm.get('sex') as FormControl;
  }
  get about() {
    return this.generalForm.get('about') as FormControl;
  }


  onSubmit(form) {
    if (form.status === 'VALID') {
      this.userService.addUser({
        firstName: this.name.value,
        lastName: this.surname.value,
        email: this.email.value,
        phone: this.phone.value,
        language: this.language.value,
        sex: this.sex.value,
        about: this.about.value
      }).subscribe(() => {
        this.notificationService.push({text: 'Successfully saved.', color: 'primary'});

        const employee = this.userService.getUser().subscribe((data) => {
          this.customer = data;
          this.generalForm.get('name').setValue(this.customer.name);
          this.generalForm.get('surname').setValue(this.customer.surname);
          this.generalForm.get('email').setValue(this.customer.email);
          this.generalForm.get('phone').setValue(this.customer.phone);
          this.generalForm.get('language').setValue(this.customer.language);
          this.generalForm.get('sex').setValue(this.customer.sex);
          this.generalForm.get('about').setValue(this.customer.about);

          const event = new CustomEvent('settingsUpdate');
          document.dispatchEvent(event);
        });

        this.requiredFields = false;
        form.reset();
      });
    } else {
      this.requiredFields = true;
    }
  }

  disabled() {
    if (this.generalForm.invalid) {
      return true;
    }
    return false;
  }
}
