import { filter } from 'rxjs/operators';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AccountsService } from '../services/accounts.service';
import { CardService } from '../services/card.service';
import { MenuService } from '../services/menu.service';
import { forkJoin } from 'rxjs';


@Component({
  selector: 'app-wrapper',
  templateUrl: './wrapper.component.html',
  styleUrls: ['./wrapper.component.scss'],
  animations: [
    trigger('toggleMenu', [

      state('full', style({
        width: '242px',
      })),

      state('mini', style({
        width: '76px',
      })),

      transition('full <=> mini', [
        animate('0.5s')
      ]),

    ])
  ]
})
export class WrapperComponent implements OnInit {

  menus;
  cards = [];
  menuStatus = true;
  title: string;
  constructor(
    private menuService: MenuService,
    private cardService: CardService,
    private activatedRoute: ActivatedRoute,
    private accountsService: AccountsService
  ) { }

  ngOnInit() {
    this.menus = this.menuService.getMenus();
    this.getAccounts();
    document.addEventListener('addCard', (e) => {
      this.getAccounts();
    }, false);
    document.addEventListener('deleteCard', (e) => {
      this.getAccounts();
    }, false);
  }

  getAccounts() {
    forkJoin(
      this.cardService.getCards(),
      this.cardService.getCredits(),
      this.cardService.getDeposits()
    )
      .subscribe((r: Array<object[]>) => {
        const accounts = [].concat.apply([], r);
        let res = accounts;
        if (accounts && accounts.length > 0) {
          res = [accounts.find((item) => item.___class === 'cards'),
          accounts.find((item) => item.___class === 'deposits'),
          accounts.find((item) => item.___class === 'credits')].filter((item) => item !== undefined);
        }
        this.cards = res;
      });
  }

  toggleSidebar() {
    this.menuStatus = !this.menuStatus;
  }

  activeRoute(e) {
    if (e.changeHeader) {
      e.changeHeader.subscribe(value => this.title = value);
      return;
    }
    this.title = this.activatedRoute.firstChild.snapshot.data.header;
  }

}
