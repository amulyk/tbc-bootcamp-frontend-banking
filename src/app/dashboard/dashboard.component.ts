import { Component, OnInit } from '@angular/core';
import { CardService } from '../services/card.service';
import { TransactionService } from '../services/transaction.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  cards;
  transactions;
  // data = {
  //   total: 0,
  //   labelsFull: [],
  //   income: [],
  //   outcome: [],
  //   labels: [],
  //   max: 20,
  //   cardIncomes: []
  // };
  data = {
    total: 900,
    labelsFull: ['May', 'June', 'Jule'],
    income: [400, 200, 300],
    outcome: [200, 400, 600],
    labels: ['May', 'Jun', 'Jul'],
    max: 700,
    cardIncomes: [
      {
        id: 'id1',
        amount: 2500
      },
      {
        id: 'id2',
        amount: 3000
      },
      {
        id: 'id3',
        amount: 1500
      },
      {
        id: 'id4',
        amount: 2500
      },
      {
        id: 'id5',
        amount: 3500
      },
      {
        id: 'id6',
        amount: 1000
      },
      {
        id: 'id7',
        amount: 4000
      },
      {
        id: 'id8',
        amount: 4500
      },
      {
        id: 'id9',
        amount: 1200
      },
      {
        id: 'id10',
        amount: 3500
      },
    ]
  };
  constructor(
    private cardService: CardService,
    private transService: TransactionService
  ) { }

  ngOnInit() {
    this.cardService.getCards().subscribe(value => this.cards = value);
    this.transService.getTransactions().subscribe(value => {
      this.transactions = value;
      const icons = [{'&#xf109': 'anchor'}, {'&#xf12d': 'bank'}, {'&#xf2af': 'online'}];

      this.transactions = this.transactions.map((trans) => {
        return { ...trans, icon: Object.values(icons.find((item) => item[trans.image] ))[0] };
      });
    });

    this.transService.getInOut().subscribe(value => {
      value.forEach(item => {
        const index = this.data.labels.findIndex(i => i === item.monthShort);
        if (index >= 0) {
          if (item.amount > 0) {
            this.data.income[index] = this.data.income[index] + item.amount;
            this.data.total = this.data.total + item.amount;
          } else {
            this.data.outcome[index] = this.data.outcome[index] + -item.amount;
          }
        } else {
          this.data.labels.unshift(item.monthShort);
          this.data.labelsFull.unshift(item.month);

          if (item.amount > 0) {
            this.data.income.unshift(item.amount);
            this.data.outcome.unshift(0);
            this.data.total = this.data.total + item.amount;
          } else {
            this.data.income.unshift(0);
            this.data.outcome.unshift(-item.amount);
          }
        }

        if (item.amount > 0) {
          const cardIndex = this.data.cardIncomes.findIndex((i) => i.id === item.cardId);
          if (cardIndex >= 0) {
            this.data.cardIncomes[cardIndex].amount = this.data.cardIncomes[cardIndex].amount + item.amount;
          } else {
            this.data.cardIncomes.push({
              id: item.cardId,
              amount: 0
            });
          }
        }

      });

    });

  }

}
