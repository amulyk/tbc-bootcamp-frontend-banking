import { Component, OnInit } from '@angular/core';
import { SettingNotificationService } from '../setting-notification.service';
import { FormBuilder, FormArray } from '@angular/forms';

@Component({
  selector: 'app-setting-note',
  templateUrl: './setting-note.component.html',
  styleUrls: ['./setting-note.component.scss']
})
export class SettingNoteComponent implements OnInit {
  checkedform;
  options;
  tags = ['Email', 'SMS', 'Telegram', 'WhatsApp'];
  constructor(
    private formBuilder: FormBuilder,
    private settingNotification: SettingNotificationService
  ) {
    this.checkedform = this.formBuilder.group({
      product: [],
      offer: [],
      comments: [],
      notification: [],
    });
    this.settingNotification.getOptions().subscribe(value => {
      this.options = value;
      this.setForm();
    });
  }
  ngOnInit() {}
  setForm() {
    this.checkedform = this.formBuilder.group({
      product: this.options.product,
      offer: this.options.offer,
      comments: this.options.comments,
      notification: this.options.notification,
    });
  }
  setOption(value) {
    this.settingNotification.setOptions(value);
  }
}
