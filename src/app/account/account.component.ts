import { Component, OnInit } from '@angular/core';
import { CardService } from '../services/card.service';
import { TransactionService } from '../services/transaction.service';
import { forkJoin } from 'rxjs';
import { Router } from '@angular/router';

interface Ioption {
  width?: number;
  height?: number;
  points?: number;
  color?: string;
  data?: {}[];
}

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss']
})
export class AccountComponent implements OnInit {
  isAllEmpty = true;
  cards = [];
  deposits = [];
  loans = [];
  transactions = null;
  chartOption: Ioption[];
  cardsBalances = [];
  firstChart: Ioption = {};
  constructor(
    private cardService: CardService,
    private transService: TransactionService,
    private router: Router
  ) { }

  ngOnInit() {
    forkJoin([
      this.cardService.getCards(),
      this.cardService.getDeposits(),
      this.cardService.getCredits()
    ])
      .subscribe(accounts => {
        this.isAllEmpty = [].concat.apply([], accounts).length === 0;

        if (!this.isAllEmpty) {
          const cards = accounts[0];
          const deposits = accounts[1];
          const credits = accounts[2];

          this.cards = cards;
          this.deposits = deposits;
          this.loans = credits;

          this.setCharts();
        } else {
          this.router.navigate(['/emptyaccount']);
        }

      });
  }

  setCharts() {
    const width = 180;
    const height = 60;

    this.firstChart.width = width;
    this.firstChart.height = height;
    this.firstChart.color = '#6DD230';
    this.firstChart.points = 0;

    this.transService.getUserTransactions().subscribe((trans: { times: []; current: string }) => {
      this.cardsBalances.push({ [trans.current]: this.total('cards') });
      trans.times.forEach((item: { time: string; amount: number }) => {
        const index = this.cardsBalances.findIndex((t) => typeof t[item.time] === 'number');
        if (index >= 0) {
          this.cardsBalances[index][item.time] += item.amount;
        } else {
          this.cardsBalances.push({
            [item.time]: Object.values(this.cardsBalances[this.cardsBalances.length - 1])[0] as number + item.amount
          });
        }
      });
      this.firstChart = {
        width,
        height,
        points: this.cardsBalances.length,
        color: '#6DD230',
        data: this.cardsBalances
      };

      this.chartOption = [
        {
          width,
          height,
          points: 5,
          color: '#6DD230',
        },
        {
          width,
          height,
          points: 5,
          color: '#FFAB2B'
        },
        {
          width,
          height,
          points: 5,
          color: '#FE4D97'
        },
      ];

    });
  }

  total(type = 'cards') {
    return this[type].reduce((sum, curr) => sum + curr.amount || 0, 0);
  }

  get averageRate() {
    return Math.round(this.deposits.reduce((sum, curr) => sum + curr.rate || 0, 0) / this.deposits.length) || 0;
  }

}
