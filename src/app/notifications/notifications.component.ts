import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { NotificationService } from '../services/notification.service';

@Component({
  selector: 'app-notification',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.scss'],
})
export class NotificationsComponent implements OnInit {
  @Input() color = 'secondary-1';
  @Input() text = 'Notification message';
  @Output() closed = new EventEmitter();

  constructor(private notificationService: NotificationService) { }

  ngOnInit() {
    setInterval(() => {
      this.notificationService.shift();
    }, 3000);
  }

  get notifications() {
    return this.notificationService.getNotifications();
  }

  close(notification) {
    this.notificationService.close(notification);
  }

}
